<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Angel',
            'last_name' => 'Hernandez',
            'email' => 'arhernandez@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('Martes.15'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Mario',
            'last_name' => 'Garcia',
            'email' => 'mgarcia@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('Early.b1rd'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Andres',
            'last_name' => 'Magaña',
            'email' => 'amagana@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('EbPa$$1'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Manuel',
            'last_name' => 'Plascencia',
            'email' => 'mplascencia@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('EbPa$$2'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Margarita',
            'last_name' => 'Golzalez',
            'email' => 'mgonzalez@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('EbPa$$3'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Jorge',
            'last_name' => 'Peña',
            'email' => 'jpena@hotel-alameda.com.mx',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('EbPa$$4'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Karen',
            'last_name' => 'Cardenas',
            'email' => 'kcardenas@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('Energiko123'),
        ]);

        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Elizabeth',
            'last_name' => 'Monrreal',
            'email' => 'amonreal@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('Energiko123'),
        ]);
        DB::connection('intranet')->table('users')->insert([
            'given_name' => 'Gabriel',
            'last_name' => 'Vazquez',
            'email' => 'gvazquez@energiko.com',
            'role' => 'admin',
            'api_token' => str_random(64),
            'password' => bcrypt('gab2018V'),
        ]);
    }
}
