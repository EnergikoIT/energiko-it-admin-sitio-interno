<?php

namespace App\Models\Arpon;

use Illuminate\Database\Eloquent\Model;

class EarlyBird extends Model
{
    protected $connection = 'arpon-front-auditoria';
    protected $table = 'EarlyBird';
}
