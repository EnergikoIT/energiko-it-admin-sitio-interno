<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Factura extends Model
{
    static function facturasTimbradas($store) {

        $year_begin = date("Y-m-d", strtotime('First day of last month' . date('Y'), time()));
        $year_end = date("Y-m-d", strtotime('Last day of December ' . date('Y'), time()));

        return DB::select("SELECT * FROM Facturas AS F LEFT JOIN FacturaCFD AS FC ON FC.FaTda_Codigo = F.FolTda_Codigo 
                           AND FC.FaEst_Codigo = F.FolEst_Codigo AND FC.FaDoc_Codigo = F.FolDoc_Codigo AND FC.FaConsecutivo = F.FolConsecutivo 
                           WHERE F.F_Cancelada = 0 AND FC.UUID IS NULL AND CONVERT(DATE,F.F_fecha) BETWEEN '$year_begin' AND '$year_end' 
                           AND F.FolTda_Codigo = $store");
    }
}
