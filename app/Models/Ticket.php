<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public static function rawSales() {
        
        $sales = DB::select(
            "SELECT CONVERT(DATE,T_Fecha) AS Fecha, SUM(T_ImporteTotal) AS Tickets 
            FROM Tickets WHERE FolTda_Codigo = 3 AND CONVERT(DATE,T_Fecha) BETWEEN '2017.01.01' AND '2017.01.09' 
            GROUP BY CONVERT(DATE,T_Fecha) 
            ORDER BY CONVERT(DATE,T_Fecha)");
        
        return $sales;
    }
}
