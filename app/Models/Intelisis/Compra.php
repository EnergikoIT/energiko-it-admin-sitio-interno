<?php

namespace App\Models\Intelisis;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $connection = 'intelisis';
    protected $table = 'Compra';
}
