<?php

namespace App\Models\Intelisis;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $connection = 'intelisis';
    protected $table = 'Venta';
}
