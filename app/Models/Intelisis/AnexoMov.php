<?php

namespace App\Models\Intelisis;

use Illuminate\Database\Eloquent\Model;

class AnexoMov extends Model
{
    protected $connection = 'intelisis';
    protected $table = 'AnexoMov';
}
