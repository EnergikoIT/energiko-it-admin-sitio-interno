<?php

namespace App\Models\Intelisis;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $connection = 'intelisis';
    protected $table = 'sucursal';
}
