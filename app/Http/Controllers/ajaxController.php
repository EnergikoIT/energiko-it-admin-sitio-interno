<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;

class ajaxController extends Controller {
    
    public function pendingInvoices($store_id = null) {

        $year_begin = date("Y-m-d", strtotime('first day of January ' . date('Y'), time()));
        $year_end = date("Y-m-d", strtotime('Last day of December ' . date('Y'), time()));

        //Obtener las facturas que faltan de timbrar
        $invoices = DB::select("SELECT
                                    CONCAT(FolTda_Codigo, '-', FolEst_Codigo, '-', FolDoc_Codigo, '-', FolConsecutivo) AS Folio,
                                    CONVERT(DECIMAL(10,2),F_ImporteTotal) as Importe,
                                    CONVERT(date, F_Fecha) as Fecha
                                FROM
                                    Facturas AS F LEFT JOIN
                                    FacturaCFD AS FC ON FC.FaTda_Codigo = F.FolTda_Codigo AND FC.FaEst_Codigo = F.FolEst_Codigo AND FC.FaDoc_Codigo = F.FolDoc_Codigo AND FC.FaConsecutivo = F.FolConsecutivo
                                WHERE
                                    F.F_Cancelada = 0 AND FC.UUID IS NULL AND CONVERT(DATE,F.F_fecha) BETWEEN '$year_begin' AND '$year_end' AND F.FolTda_Codigo = $store_id");
        
        return response()->json($invoices); 

    }
}
