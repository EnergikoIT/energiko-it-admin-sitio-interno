<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Intelisis\Compra;

class comprasController extends Controller {

    public function pendingAsoc() {
        $pending_notes =  Compra::take(100)->get();

        return view('tools.pending');
    }

    public function alreadyAsoc() {
        $pending_notes =  null;

        return view('tools.asociated');
    }
}
