<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\Models\Vacante;


class rhController extends Controller
{
    public function index() {

        $vacancys = Vacante::all();
        return view('rh.vacantes')->with('vacancys', $vacancys);
    }

    public function edit($id) {
        $vacancy = Vacante::find($id);

        return view('rh.vacante')->with('vacancy', $vacancy);
    }

    public function create() {

        return view('rh.nueva_vacante');
    }

    public function guardar(Request $request) {

        $vacancy = new Vacante();
        
        $vacancy->habilitar = $request->has('habilitar')? true : false; 
        $vacancy->nombre = $request->nombre;
        $vacancy->descripcion = $request->descripcion;
        $vacancy->division = $request->division;
        $vacancy->localidad = $request->localidad;
        $vacancy->tipo_puesto = $request->tipo_puesto;
        $vacancy->empresa = $request->empresa;
        $vacancy->sexo = $request->sexo;
        $vacancy->edad = $request->edad;
        $vacancy->escolaridad = $request->escolaridad;
        $vacancy->disponibilidad = $request->disponibilidad;
        $vacancy->tipo_vacante = $request->tipo_vacante;
        $vacancy->contratacion = $request->contratacion;
        $vacancy->experiencia = $request->experiencia;

        if($vacancy->save()) {
            return redirect('rh/vacantes');
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function actualizar(Request $request) {

        $vacancy = Vacante::find($request->vacancy_id);

        $vacancy->habilitar = $request->has('habilitar')? true : false; 
        $vacancy->nombre = $request->nombre;
        $vacancy->descripcion = $request->descripcion;
        $vacancy->division = $request->division;
        $vacancy->localidad = $request->localidad;
        $vacancy->tipo_puesto = $request->tipo_puesto;
        $vacancy->empresa = $request->empresa;
        $vacancy->sexo = $request->sexo;
        $vacancy->edad = $request->edad;
        $vacancy->escolaridad = $request->escolaridad;
        $vacancy->disponibilidad = $request->disponibilidad;
        $vacancy->tipo_vacante = $request->tipo_vacante;
        $vacancy->contratacion = $request->contratacion;
        $vacancy->experiencia = $request->experiencia;

        if($vacancy->save()) {
            return redirect('rh/vacantes');
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function eliminar(Request $request) {

        $vacancy = Vacante::find($request->vacancy_id);

        if($vacancy->delete()) {
            return redirect('rh/vacantes');
        } else {
            return redirect()->back()->withInput();
        }

    }
}
