<?php 
namespace App\Http\Controllers;

use DB;
use Excel;
use Route;

use App\Models\Ticket;
use App\Models\TicketDevolucion;
use App\Models\Factura;
use App\Models\NotasCredito;
use App\Models\Tienda;
use App\Models\CortesZ;
use App\Models\Intelisis\Sucursal;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class accountancyController extends BaseController {

    public function estatusTiendas(Request $request) {

        if(!$request->from_date)
            $from_date = date("Y-m-d", strtotime('first day of this month', time())) . ' 00:00:00';
        else 
            $from_date = $request->from_date . ' 00:00:00';

        if(!$request->to_date)
            $to_date = date("Y-m-d", strtotime('last day', time())) . ' 23:59:59';
        else 
            $to_date = $request->to_date . ' 23:59:59';

            
        $stores = Tienda::where('Tda_Codigo', '>', 2)->whereNotIn('Tda_Codigo', [4,13,16,17,18,19,21,22,23,24,25,26])->get(['Tda_Codigo', 'Tda_Nombre']);
        $total_venta = $total_facturado = $total_diferencia = 0;

        foreach($stores as $store) {

            $data[$store->Tda_Codigo]['nombre'] = str_replace('T. ','',$store->Tda_Nombre);
            
            $data[$store->Tda_Codigo]['venta'] = (float)Ticket::whereBetween('T_Fecha', [$from_date, $to_date])
                ->where('FolTda_Codigo', $store->Tda_Codigo)->sum('T_ImporteTotal');
            
            $data[$store->Tda_Codigo]['devoluciones'] = (float)TicketDevolucion::whereBetween('TD_Fecha', [$from_date, $to_date])
                ->where('FolTda_Codigo', $store->Tda_Codigo)->sum('TD_TotalImporte');

            $data[$store->Tda_Codigo]['venta_neta'] = $data[$store->Tda_Codigo]['venta'] - $data[$store->Tda_Codigo]['devoluciones'];
            
            $data[$store->Tda_Codigo]['facturado'] = (float)Factura::whereBetween('F_Fecha', [$from_date, $to_date])
                ->where('FolTda_Codigo', $store->Tda_Codigo)->where('F_Cancelada', 0)->sum('F_ImporteTotal');
            
            $data[$store->Tda_Codigo]['notas_credito'] = (float)NotasCredito::whereBetween('NC_Fecha', [$from_date, $to_date])
                ->where('FolTda_Codigo', $store->Tda_Codigo)->sum('NC_TotalImporte');

            $data[$store->Tda_Codigo]['facturacion_neta'] = $data[$store->Tda_Codigo]['facturado'] - $data[$store->Tda_Codigo]['notas_credito'];
            
            $data[$store->Tda_Codigo]['diferencia_ventas'] =  round( $data[$store->Tda_Codigo]['venta_neta'] - $data[$store->Tda_Codigo]['facturacion_neta'] );

            $data[$store->Tda_Codigo]['timbrado'] = count(Factura::facturasTimbradas($store->Tda_Codigo));

            $data[$store->Tda_Codigo]['sincro'] = substr(DB::table('BitacoraDiscon')->where('BDC_Tda',  $store->Tda_Codigo)->max('BDC_Fecha') ,0, 10);
        }

        return view('reports.results')->with('data', $data)->with('from_date', explode(' ', $from_date)[0] )->with('to_date', explode(' ', $to_date)[0]);
    }

    public function cortesz() {

        $data = DB::select("SELECT E.Tda_Codigo, E.Est_Codigo, (SELECT MAX(CONVERT(DATE,T.T_Fecha)) FROM Tickets AS T WHERE T.FolTda_Codigo = E.Tda_Codigo AND T.FolEst_Codigo = E.Est_Codigo AND CONVERT(DATE,T.T_Fecha) > '2017-01-01') AS MAX_T, (SELECT MAX(CONVERT(DATE,CZ.CZ_Fecha)) FROM CortesZ AS CZ WHERE CZ.FolTda_Codigo = E.Tda_Codigo AND CZ.FolEst_Codigo = E.Est_Codigo AND CONVERT(DATE,CZ.CZ_Fecha) > '2017-01-01') AS MAX_CZ
                            FROM Estaciones AS E WHERE E.Tda_Codigo > 2 AND E.Tda_Codigo <> 4 AND E.Tda_Codigo <> 13 AND E.Tda_Codigo <> 16 AND E.Tda_Codigo <> 17 AND E.Tda_Codigo <> 18 AND E.Tda_Codigo <> 19 AND E.Tda_Codigo <> 21 AND E.Tda_Codigo <> 22 AND E.Tda_Codigo <> 23 AND E.Tda_Codigo <> 24 AND E.Tda_Codigo <> 25 AND E.Tda_Codigo <> 26
                            ORDER BY E.Tda_Codigo, E.Est_Codigo");
       
        foreach($data as $key => $store) {
             $store->nombre = str_replace('T. ','', Tienda::where('Tda_Codigo', $store->Tda_Codigo)->first()->Tda_Nombre);
             //$store->intelisis_id = Sucursal::where('Nombre', 'LIKE', $store->nombre)->whereNotIn('Sucursal', [36])->first()->Sucursal;
        }
        
        return view('reports.cortesz')->with('data', $data);
    }

    public function analisis() {
        return view('reports.analisis');
    }

    public function reportesHome() {

        $from_date = date("Y-m-d", strtotime('first day of last month', time())) . ' 00:00:00';
        $to_date = date("Y-m-d", strtotime('last day of last month', time())) . ' 23:59:59';
        
        $stores = Tienda::where('Tda_Codigo', '>', 2)->whereNotIn('Tda_Codigo', [4,13,16,17,18,19,21,22,23,24,25,26])->get(['Tda_Codigo', 'Tda_Nombre']);

        return view('reports.reportes')->with('stores', $stores)->with('from_date', explode(' ', $from_date)[0] )->with('to_date', explode(' ', $to_date)[0]);
    }
    
}
