<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\Models\Arpon\EarlyBird;

use Auth;
use DB;
use Redirect;


class apiController extends Controller {

    public function logIn(Request $request) {

        $credentials = [
            'email' => $request->email, 
            'password' => $request->password
        ];
 
        if(Auth::once($credentials)) {
            return  response()->json([
                'status' => 'success',
                'user' => $request->user()
            ]);
        } 
        else {
            return response()->json([
                'status'=>'error', 
                'message' => 'Usuario y/o contraseña incorrectos'
            ]);
        }
    }

    public function jobs($div = null, $loc = null, $tp = null) {

        $vacantes = DB::connection('intranet')->table('vacantes')
            ->when($div, function ($query) use ($div) {
                return $query->where('division', $div);
            })
            ->when($loc, function ($query) use ($loc) {
                return $query->where('localidad', $loc);
            })
            ->when($tp, function ($query) use ($tp) {
                return $query->where('tipo_puesto', $tp);
            })
            ->where('habilitar', 1)->get();

        return response()->json($vacantes);
    }

    public function contact(Request $request) {

        $mail = new \PHPMailer;
        $mail->isSMTP();     
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = env('MAIL_USERNAME', null);
        $mail->Password = env('MAIL_PASSWORD', null);
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom('contacto@energiko.com', 'Grupo Energiko');
        $mail->addReplyTo($request->email, $request->name);
        
        $mail->addAddress($request->to_email_1);
        
        if($request->has('to_email_2')) 
            $mail->addAddress($request->to_email_2); 
        
        if($request->hasFile('fileToUpload')){
            $destination = 'uploads/resumes';
            $file = $request->file('fileToUpload');
            $filename = 'temp_cv.' . $file->extension();
            $file->move($destination, $filename);

            $mail->addAttachment(public_path($destination . '/' . $filename));
        }

        $mail->isHTML(true);    
        $mail->Subject = $request->subject;

        if($request->subject == 'Nuevo curriculum en Energiko')
            $mail->Body = "Un nuevo Curriculm ha sido recibido en la bolsa de trabajo de energiko.com <br> Los datos que el aplicante ingreso son los siguientes: <br><br> <b>Nombre:</b> $request->name <br> <b>Telefono:</b> $request->phone <br> <b>Email:</b> $request->email <br> <b>Mensaje:</b> <i> $request->message </i>";
            //$mail->Body = view('mails.CVHappyGo')->with('request', $request);
        else if($request->subject == 'Contacto en Energiko') {
            $mail->Body = "Una persona esta interesada en ponerse en contacto con Energiko <br> Los datos que el interesado ingreso son los siguientes: <br><br> <b>Nombre:</b> $request->name <br> <b>Telefono:</b> $request->phone <br> <b>Email:</b> $request->email <br> <b>Mensaje:</b> <i> $request->message </i>";
        }
        else {
            return 'none';
        }

        if(!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return back();
        }
    }

    //Informacion del dia anterior
    public function earlyBird() {
       $fields = [
            'Fecha',
            'Rubro',
            'Descripcion',
            DB::raw("CONVERT(DECIMAL(10,2),TotalDia) AS TotalDia"),
            'PorcentajeDia',
            DB::raw("CONVERT(DECIMAL(10,2),TotalMes) AS TotalMes"),
            'PorcentajeMes',
            DB::raw("CONVERT(DECIMAL(10,2),TotalAño) AS TotalAnio"),
            'PorcentajeAño AS PorcentajeAnio',
            'PresDia',
            'PorcentajePresDia',
            'PresMes',
            'PorcentajePresMes',
            'PresAño AS PresAnio',
            'PorcentajePresAño AS PorcentajePresAnio'
        ];       
       

        

        $data = EarlyBird::take(33)->orderby('Fecha', 'desc')->orderby('Rubro', 'asc')->get($fields)->groupBy('Rubro');        
        
        return $data->toJson();
    }

    public function ebMonthHistory() {
        
        $today = date('Y-m-d');
        $one_month_ago = date('Y-m-d', strtotime('-1 months', strtotime($today)));
        $two_month_ago = date('Y-m-d', strtotime('-2 months', strtotime($today))); 

        $fields = [
            DB::raw("CONVERT(varchar(3), DATENAME(MONTH, Fecha)) AS Mes"),
            'Fecha',
            'Rubro',
            'Descripcion',
            DB::raw("CONVERT(DECIMAL(10,2),TotalDia) AS TotalDia"),
            'PorcentajeDia',
            DB::raw("CONVERT(DECIMAL(10,2),TotalMes) AS TotalMes"),
            'PorcentajeMes',
            DB::raw("CONVERT(DECIMAL(10,2),TotalAño) AS TotalAnio"),
            'PorcentajeAño AS PorcentajeAnio',
            'PresDia',
            'PorcentajePresDia',
            'PresMes',
            'PorcentajePresMes',
            'PresAño AS PresAnio',
            'PorcentajePresAño AS PorcentajePresAnio'
        ];

        $data['current'] = EarlyBird::where('Rubro', 'R29')->take(33)->orderby('Fecha', 'desc')->orderby('Rubro', 'asc')->first($fields);  
        $data['one_month_ago'] = EarlyBird::where('Rubro', 'R29')->where('Fecha', $one_month_ago)->take(33)->orderby('Fecha', 'desc')->orderby('Rubro', 'asc')->first($fields);
        $data['two_month_ago'] = EarlyBird::where('Rubro', 'R29')->where('Fecha', $two_month_ago)->take(33)->orderby('Fecha', 'desc')->orderby('Rubro', 'asc')->first($fields);

        $data['day_top_3'] = EarlyBird::whereNotIn('Rubro', ['R29', 'R61', 'R62'])->take(5)->orderby('Fecha', 'desc')->orderby('TotalDia', 'desc')->get($fields);
        $data['month_top_3'] = EarlyBird::whereNotIn('Rubro', ['R29', 'R61', 'R62'])->take(5)->orderby('Fecha', 'desc')->orderby('TotalMes', 'desc')->get($fields);
        $data['year_top_3'] = EarlyBird::whereNotIn('Rubro', ['R29', 'R61', 'R62'])->take(5)->orderby('Fecha', 'desc')->orderby('TotalAnio', 'desc')->get($fields);

        return $data;
    }

    public function hotelComparative($period = null) {
        //date("Y-m-d", strtotime('First day of last month' . date('Y'), time()));
        
        $inicio = date("Y-m-d", strtotime('yesterday', time()));
        $fin = date("Y-m-d", strtotime('yesterday', time()));
        

        if(!$period) { //dia
            $hotels_data = DB::connection('arpon')->select(
                "SELECT dispo.HOTEL as hotel, lower(comp.NOMBREH) as nombre, CTOS_DISPONIB as ctos_disponib, CTOS_VENDIDOS as ctos_vendidos, TARIFA_PROM as tarifa_prom 
                FROM DISPO_VEND_TAR dispo INNER JOIN comparativo_hoteles comp on comp.HOTEL = dispo.HOTEL
                WHERE ([FECHA]>='$inicio' AND [FECHA]<='$fin')"
            ); 
        }
        else { 
            //mes o año
            if($period == 'month') { 
                $inicio = date("Y-m-d", strtotime('first day of this month', time()));
                $fin = date("Y-m-d", strtotime('yesterday', time()));
            } else {
                
                $inicio = date("Y-m-d", strtotime('first day of January', time()));
                $fin = date("Y-m-d", strtotime('yesterday', time()));
            }

            $hotels_data = DB::connection('arpon')->select(
                "SELECT dispo.HOTEL as hotel, lower(comp.NOMBREH) as nombre, sum(CTOS_DISPONIB) as ctos_disponib, sum(CTOS_VENDIDOS) as ctos_vendidos, AVG(TARIFA_PROM) as tarifa_prom 
                FROM DISPO_VEND_TAR dispo INNER JOIN comparativo_hoteles comp on comp.HOTEL = dispo.HOTEL
                WHERE ([FECHA]>='$inicio' AND [FECHA]<='$fin')
                GROUP BY dispo.HOTEL, comp.NOMBREH"
            ); 
        }

        $totals_row = DB::connection('arpon')->select(
            "SELECT 99 AS hotel, 'Totales' AS nombre, SUM(CTOS_DISPONIB) AS ctos_disponib, SUM(CTOS_VENDIDOS) AS ctos_vendidos , AVG(TARIFA_PROM) AS tarifa_prom, 0 as variacion
            FROM DISPO_VEND_TAR 
            WHERE ([FECHA]>='$inicio' AND [FECHA]<='$fin')"
        )[0];
        
        $tot_part_merca = $tot_merc_logra = $tot_ocup_hotel = $tot_ingresos = $tot_rev_par = 0;

        foreach($hotels_data as $key => $hotel) {
            $hotel->nombre = ucwords($hotel->nombre);
            $hotel->merc_parte = number_format((100 * $hotel->ctos_disponib) / $totals_row->ctos_disponib, 2);
            $hotel->ocup_hotel = number_format((100 * $hotel->ctos_vendidos) / $hotel->ctos_disponib, 2);
            $hotel->merc_logra = number_format((100 * $hotel->ctos_vendidos) / $totals_row->ctos_vendidos, 2);
            $hotel->variacion = number_format($hotel->merc_parte - $hotel->merc_logra, 2);
            $hotel->ingresos = $hotel->ctos_vendidos * $hotel->tarifa_prom;
            $hotel->rev_par = number_format($hotel->ingresos / $hotel->ctos_disponib, 2);
            $hotel->is_top = (int)$hotel->ocup_hotel > 90? 1 : 0;

            //totales calculados
            $tot_part_merca += $hotel->merc_parte;
            $tot_ocup_hotel += $hotel->ocup_hotel;
            $tot_merc_logra += $hotel->merc_logra;
            $tot_ingresos += $hotel->ingresos;
            $tot_rev_par += $hotel->rev_par;

            //Cambiamos el formato de estos valores una vez que ya fueron utilizados para los calculos aritmeticos
            $hotel->tarifa_prom = number_format($hotel->tarifa_prom, 2);
            $hotel->ingresos = number_format($hotel->ingresos, 2);            
        }
        
        $hotels_number = count($hotels_data);
        $totals_row->merc_parte = number_format($tot_part_merca);
        $totals_row->ocup_hotel = number_format($tot_ocup_hotel/$hotels_number, 2);
        $totals_row->merc_logra = number_format($tot_merc_logra);
        $totals_row->tarifa_prom = number_format($totals_row->tarifa_prom, 2);
        $totals_row->ingresos = number_format($tot_ingresos, 2);
        $totals_row->rev_par = number_format($tot_rev_par / $hotels_number, 2);

        //Agregamos una columna de totales al final del arreglo de hoteles
        $totals = ((array)($totals_row));
        $hotels_data[$hotels_number] = $totals;

        return response()->json(['data' => $hotels_data]);
    }
}
