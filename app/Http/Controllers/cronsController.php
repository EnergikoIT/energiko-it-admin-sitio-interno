<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\Models\Tienda;
use DB;
use SoapClient;

class cronsController extends Controller {

    public function czAlertMail() {
        //Cargamos el directorio de tiendas
        require public_path('directorio.php');
        
        $today = date('d');
        $lastday = date('t',strtotime('today'));
        $last_five_days_of_month = [(int)$lastday,$lastday-1,$lastday-2];

        $data = DB::select("SELECT E.Tda_Codigo, E.Est_Codigo, 
                                (SELECT MAX(CONVERT(DATE,T.T_Fecha)) FROM Tickets AS T WHERE T.FolTda_Codigo = E.Tda_Codigo AND T.FolEst_Codigo = E.Est_Codigo AND CONVERT(DATE,T.T_Fecha) > '2017-01-01') AS MAX_T, 
                                (SELECT MAX(CONVERT(DATE,CZ.CZ_Fecha)) FROM CortesZ AS CZ WHERE CZ.FolTda_Codigo = E.Tda_Codigo AND CZ.FolEst_Codigo = E.Est_Codigo AND CONVERT(DATE,CZ.CZ_Fecha) > '2017-01-01') AS MAX_CZ,
                                (SELECT MAX(CONVERT(date,BDC_Fecha)) from BitacoraDiscon AS BD WHERE BD.BDC_Tda = E.Tda_Codigo) AS MAX_SINCRO
                            FROM Estaciones AS E WHERE E.Tda_Codigo > 2
                            ORDER BY E.Tda_Codigo, E.Est_Codigo");

        foreach($data as $key => $corte) {
            $corte->tienda = str_replace('T. ','', Tienda::where('Tda_Codigo', $corte->Tda_Codigo)->first()->Tda_Nombre);
            
            if( (($corte->MAX_CZ  < $corte->MAX_T) && ($corte->MAX_CZ !== date('Y-m-d', strtotime('yesterday')))) && $corte->MAX_SINCRO == date('Y-m-d') ) {
                 //El directorio no contempla tienda 18 y 20

                 if(isset($directorio[$corte->Tda_Codigo])) { 
                    
                    $manager = explode(',', $directorio[$corte->Tda_Codigo]);
                    try {
                        $mail = new \PHPMailer;
                        $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                        $mail->isSMTP();     
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = env('MAIL_USERNAME', null);
                        $mail->Password = env('MAIL_PASSWORD', null);
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port = 465;

                        $mail->setFrom('sistemas@energiko.com', 'Grupo Energiko');
                        $mail->addAddress($manager[0]);
                        $mail->addAddress($manager[1]);
                        // Unicamente recibe el correo los 3 dias anteriores al cierre de mes
                        if(in_array($today, $last_five_days_of_month))
                            $mail->addAddress('lfreynals@energiko.com');
                        
                        $mail->addAddress('aestrella@energiko.com');
                        $mail->addAddress('mplascencia@energiko.com');  
                        $mail->addAddress('kcardenas@energiko.com');  

                        $mail->isHTML(true);    
                        $mail->Subject = '[Alerta] Corte Z no generado';
                        //Usamos una vista para generar un HTML como cuerpo del mail
                        $mail->Body = view('mails.CZAlertMail')->with('corte', $corte);

                        if(!$mail->send()) {
                            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                            echo 'El correo ha sido enviado';
                        }
                    }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
                }
            } 
            else{
                echo "T$corte->Tda_Codigo-E$corte->Est_Codigo: Nada por enviar, revisar sincronizaciones. \r\n";
            }
        }
    }


    public function invoicesAlertMail() {

        require public_path('directorio.php');

        $today = date('d');
        $lastday = date('t',strtotime('today'));
        $last_five_days_of_month = [(int)$lastday,$lastday-1,$lastday-2];
        
        $period_begin = date("Y-m-d", strtotime('First day of last month' . date('Y'), time()));
        $period_end = date("Y-m-d", strtotime('Last day of December ' . date('Y'), time()));

        //Obtenemos las facturas que no se han timbrado y las agrupamos en un arreglo de objetos por tienda
        $data = DB::table('Facturas')->leftJoin('FacturaCFD', function ($join) {
            $join->on('Facturas.FolTda_Codigo', '=', 'FacturaCFD.FaTda_Codigo')
                 ->on('Facturas.FolEst_Codigo', '=', 'FacturaCFD.FaEst_Codigo')
                 ->on('Facturas.FolDoc_Codigo', '=', 'FacturaCFD.FaDoc_Codigo')
                 ->on('Facturas.FolConsecutivo', '=', 'FacturaCFD.FaConsecutivo');
        })
        ->whereNull('FacturaCFD.UUID')
        ->where('Facturas.F_Cancelada', 0)
        ->whereRaw("CONVERT(DATE,Facturas.F_fecha) BETWEEN '$period_begin' AND '$period_end'")
        ->get()
        ->groupBy('FolTda_Codigo');
       
        foreach($data as $store => $store_invoices) {
            //Obtener la ultima sincronizacion de la tienda 
            $sincro = DB::select(DB::raw("select MAX(CONVERT(date,BDC_Fecha)) as MAX_SINCRO from BitacoraDiscon AS BD WHERE BD.BDC_Tda = $store"));

            //El directorio no contempla tienda 18 y 20
            if(isset($directorio[$store]) && $sincro[0]->MAX_SINCRO == date('Y-m-d')) { 
                $manager = explode(',', $directorio[$store]);
                try {
                    $mail = new \PHPMailer;
                    $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                    $mail->isSMTP();    
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = env('MAIL_USERNAME', null);
                    $mail->Password = env('MAIL_PASSWORD', null);
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port = 465;

                    $mail->setFrom('sistemas@energiko.com', 'Grupo Energiko');

                    // Unicamente recibe el correo los 5 dias anteriores al cierre de mes
                    if(in_array($today, $last_five_days_of_month))
                        $mail->addAddress('lfreynals@energiko.com');
                        
                    $mail->addAddress($manager[0]);
                    $mail->addAddress($manager[1]);
                    $mail->addAddress('aestrella@energiko.com');
                    $mail->addAddress('mplascencia@energiko.com');
                    $mail->addAddress('kcardenas@energiko.com');
                    $mail->isHTML(true);    
                    $mail->Subject = '[Alerta] Facturacion sin timbrar';
                    //Usamos una vista para generar un HTML como cuerpo del mail
                    $mail->Body = view('mails.InvoiceAlertMail')->with('store_invoices', $store_invoices);

                    if(!$mail->send()) {
                        throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                    } else {
                        echo 'El correo ha sido enviado';
                    }
                }
                catch(Exception $e) {
                    echo 'Message: ' .$e->getMessage();
                }
            }
        }
    }
    public function AlertAgents()
    {
         $data = DB::connection('potogas')->table("tzOperQueue")->SelectRaw("PlantaID as Agente,Count(*) as Transacciones")
         ->whereIn("Status",['P','D'])->groupby("PlantaId")->havingRaw('Count(*)>40')->get()->groupby("Agente"); 
         $validation=0;     
         foreach($data as $key => $agent )
         {
           foreach($agent as $element)
           {
                switch(str_replace(" ","",$element->Agente))
                {
                    case "02": if(((integer)$element->Transacciones)>=200)
                    {
                        $validation=1;
                    }
                    break;                   
                }
           }
        }       
       if($validation==1 or sizeof($data)>0 )
        {
            try {
                    $mail = new \PHPMailer;
                    $mail->isSMTP();                     
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = env('MAIL_USERNAME', null);
                    $mail->Password = env('MAIL_PASSWORD', null);
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port = 465;

                    $mail->setFrom('amonreal@energiko.com', 'Departamento de IT Energiko');
                    $mail->addAddress('amonreal@energiko.com');
                    $mail->isHTML(true);    
                    $mail->Subject = '[Alerta] Retraso de Agentes';
                    $mail->Body = view('mails.AgentesAlertMail')->with('dato',$data);

                    if(!$mail->send()) {
                        throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                    } else {
                        echo 'El correo ha sido enviado';
                    }
                }
                catch(Exception $e) {
                    echo 'Message: ' .$e->getMessage();
                } 
        } 
    }    

    public function AlertTransactionSuspend()
    {
         $data = DB::connection('potogas')->table("tzOperQueue")->SelectRaw("PlantaID as Agente, OperNbr,Fecha")
         ->whereIn("Status",['S'])->get(); 
         if(sizeof($data)>0)
         {
            try {
                    $mail = new \PHPMailer;
                    $mail->isSMTP();   
                    $mail->Debug=2;  
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = env('MAIL_USERNAME', null);
                    $mail->Password = env('MAIL_PASSWORD', null);
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port = 465;

                    $mail->setFrom('amonreal@energiko.com', 'Departamento de IT Energiko');
                    $mail->addAddress('amonreal@energiko.com');
                    $mail->isHTML(true);    
                    $mail->Subject = '[Alerta] Transacciones Suspendidas del Agente';
                    $mail->Body = view('mails.TransactionSuspendAlertMail')->with('data',$data);

                    if(!$mail->send()) {
                        throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                    } else {
                        echo 'El correo ha sido enviado';
                    }
                }
                catch(Exception $e) {
                    echo 'Message: ' .$e->getMessage();
                } 
         }       
    }
    /*
    Alerta para revisar la carga de trabajo del administrador de procesos DISA
    */
    public function AlertaAdminDISA()
    {
        require public_path('directorio.php');
        $data = DB::connection('disaapp')->table("processqueue")->SelectRaw("CpnyId,Count(1) as Transacciones,min(Crtd_DateTime) as FechaAntigua")
        ->WhereIn("processpriority",[115,116,117,118,119,120,155,255,290])
        ->groupby("CpnyID")
        ->orderby('Transacciones','desc')
        ->get(); 
        
        $lim=600;
        $total=0;

        foreach ($data as $key => $index) {
           $total=$total+$index->Transacciones;
        }
        if($total>$lim )
        {
            foreach ($team_aplicaciones as $key => $value) {  
                $actual=date("H:i:s");
                if(($actual>=$value['HoraInicial'] && $actual<=$value['HoraFinal']) || $actual>=$value['Extra'])
                { 
                    try {
                        $mail = new \PHPMailer;
                        $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                        $mail->isSMTP();                   
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = env('MAIL_USERNAME', null);
                        $mail->Password = env('MAIL_PASSWORD', null);                         
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port = 465;
                        $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');          
                        $mail->addAddress( $value['Correo']);                                         
                        $mail->isHTML(true);    
                        $mail->Subject = '[Alerta] Administrador de Procesos: Sin Procesar ';
                        $mail->Body = view('mails.AlertAdminProcess')->with('data',$data);                                                           

                        if(!$mail->send()) {
                            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                            echo 'El correo ha sido enviado';
                        }
                    }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
                }
            }
        }
        echo 'No existe retraso en el procesamiento: '.sizeof($data);
    }   
    
    /*
    Alerta para validar que el adminsitrador de procesos DISA no presente error de procesamiento en algún movimiento.
    */ 
     public function AlertErrorAdminDISA()
    {
        require public_path('directorio.php');
        $data = DB::connection('disaapp')->table("SONoUpdate")->SelectRaw("CpnyId,Crtd_DateTime,ShipperID,MessageText")
        ->whereRaw("MessageText<>'Embarque ya no está abierto.'")->get();
        if(sizeof($data)>0 )
        {
            foreach ($team_aplicaciones as $key => $value) {  
                $actual=date("H:i:s");
                if(($actual>=$value['HoraInicial'] && $actual<=$value['HoraFinal']) || $actual>=$value['Extra'])
                {                   
                    try {
                        
                        $mail = new \PHPMailer;
                        $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                        $mail->isSMTP();                   
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = env('MAIL_USERNAME', null);
                        $mail->Password = env('MAIL_PASSWORD', null);                         
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port = 465;
                        $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');          
                        $mail->addAddress( $value['Correo']);                                         
                        $mail->isHTML(true);    
                        $mail->Subject = '[Alerta DISA] Administrador de Procesos: Error';
                        $mail->Body = view('mails.AlertErrorAdmin')->with('data',$data);

                        if(!$mail->send()) {
                            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                            echo 'El correo ha sido enviado';
                        }
                    }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
                }
            }
        }
        else
            {echo 'No presenta errores el adminsitrador de procesos';}
    }
        
    /*
    Alerta para validar que el adminsitrador de procesos DISA este abierto.
    */ 
    public function AlerOpenAdminDISA()
    {
        require public_path('directorio.php');
        $data = DB::connection('disasys')->table("Access")->SelectRaw("*")
        ->whereRaw("ScrnNbr='40400'")->get();
        if(sizeof($data)==0 )
        {
            foreach ($team_aplicaciones as $key => $value) {  
                $actual=date("H:i:s");
                if(($actual>=$value['HoraInicial'] && $actual<=$value['HoraFinal']) || $actual>=$value['Extra'])
                {                   
                    try {
                        
                        $mail = new \PHPMailer;
                        $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                        $mail->isSMTP();                   
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = env('MAIL_USERNAME', null);
                        $mail->Password = env('MAIL_PASSWORD', null);                         
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port = 465;
                        $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');          
                        $mail->addAddress( $value['Correo']);                                         
                        $mail->isHTML(true);    
                        $mail->Subject = '[Alerta DISA] Administrador de Procesos: Detenido';
                        $mail->Body = view('mails.AlertOpenAdmin')->with('data',$data);

                        if(!$mail->send()) {
                            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                            echo 'El correo ha sido enviado';
                        }
                    }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
                }
            }
        }
        else
            {echo 'No presenta errores el adminsitrador de procesos';}
    }
    /*
    Alerta de movimientos duplicados en el administrador de procesos
    */
    public function AlertDuplicadoaAdminDISA()
    {
        require public_path('directorio.php');
        $data = DB::connection('disaapp')->table("ProcessQueue")
        ->join('vs_company','ProcessQueue.CpnyID','=','vs_company.CpnyID')
        ->SelectRaw("ProcessQueue.SOOrdNbr AS OrdenVenta,ProcessQueue.ProcessType AS TipoProceso,ProcessQueue.ProcessPriority AS Prioridad ,ProcessQueue.CpnyID AS Cedi,vs_company.CpnyName as NombreCedi,ProcessQueue.SOShipperID AS Embarque,ProcessQueue.SOLineRef AS Linea,ProcessQueue.PONbr AS OrdenCompra,ProcessQueue.POLineRef AS LineaOV,ProcessQueue.Crtd_Prog AS Pantalla,COUNT(*) AS Movimientos")
        ->GroupBy('ProcessQueue.SOOrdNbr','ProcessQueue.ProcessType','ProcessQueue.ProcessPriority','ProcessQueue.CpnyID','vs_company.CpnyName','ProcessQueue.SOShipperID','ProcessQueue.SOLineRef','ProcessQueue.PONbr','ProcessQueue.POLineRef','ProcessQueue.Crtd_Prog')
        ->HavingRaw('COUNT(*)>1')
        ->OrderBy('ProcessQueue.SOOrdNbr')
        ->get();
        
        if(sizeof($data)>0 )
        {
            try {                        
                $mail = new \PHPMailer;
                $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                $mail->isSMTP();                   
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->Username = env('MAIL_USERNAME', null);
                $mail->Password = env('MAIL_PASSWORD', null);                         
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;
                $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');          
                $mail->addAddress('amonreal@energiko.com','llugo@tezegdl.com.mx');                                        
                $mail->isHTML(true);    
                $mail->Subject = '[Alerta DISA] Administrador de Procesos: Transacciones duplicadas';
                $mail->Body = view('mails.AlertDuplicadoAdmin')->with('data',$data);

                if(!$mail->send()) {
                    throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                } else {
                    echo 'El correo ha sido enviado';
                }
            }
            catch(Exception $e) {
                echo 'Message: ' .$e->getMessage();
            }
        }
        else{echo 'No presenta errores el administrador de procesos';}

       /* if(sizeof($data)>0 )
        {
            foreach ($team_aplicaciones as $key => $value) {  
                $actual=date("H:i:s");
                if(($actual>=$value['HoraInicial'] && $actual<=$value['HoraFinal']) || $actual>=$value['Extra'])
                {                   
                    try {
                        
                        $mail = new \PHPMailer;
                        $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                        $mail->isSMTP();                   
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = env('MAIL_USERNAME', null);
                        $mail->Password = env('MAIL_PASSWORD', null);                         
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port = 465;
                        $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');          
                        $mail->addAddress('amonreal@energiko.com');                                        
                        $mail->isHTML(true);    
                        $mail->Subject = '[Alerta DISA] Administrador de Procesos: Transacciones duplicadas';
                        $mail->Body = view('mails.AlertDuplicadoAdmin')->with('data',$data);

                        if(!$mail->send()) {
                            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                            echo 'El correo ha sido enviado';
                        }
                    }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
                }
            }
        }
        else
            {echo 'No presenta errores el administrador de procesos';}*/
    }
    /*
    Alerta de Timbrado
    */
    public function AlertTimbrado()
    {
        
        require public_path('directorio.php');
        $client = new SoapClient("https://solucionfactible.com/ws/services/Utilerias?wsdl");
        foreach($empresas as $key=>$element)
        {
        $params = array('usuario' => $element['Usuario'], 'password' => $element['Password'], 'rfcEmisor' => $element['RFC']);
        $response = $client->__soapCall('getTimbres', array('parameters' => $params)); 
        $ret = $response->return;              
        $data=array('usados'=>number_format($ret->usados),'contratados'=>number_format($ret->contratados),'restantes'=>number_format($ret->contratados-$ret->usados),'empresa'=>$element['Empresa']);   
            if(isset($dir_tim[$element['RFC']]) && ($ret->contratados-$ret->usados)<=$element['Limite'] && $ret->contratados>0 ) { 
                $manager = explode(',', $dir_tim[$element['RFC']]);
                try {
                            $mail = new \PHPMailer;
                            $mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
                            $mail->isSMTP();                   
                            $mail->Host = 'smtp.gmail.com';
                            $mail->SMTPAuth = true;
                            $mail->Username = env('MAIL_USERNAME', null);
                            $mail->Password = env('MAIL_PASSWORD', null);                         
                            $mail->SMTPSecure = 'ssl';
                            $mail->Port = 465;
                            $mail->setFrom('amonreal@energiko.com', 'Grupo Energiko Departamento IT');
                            $mail->addAddress('amonreal@energiko.com');
                            $mail->addAddress('mplascencia@energiko.com');
                            $mail->addAddress($dir_tim[$element['RFC']]);                   
                            $mail->isHTML(true);    
                            $mail->Subject = '[Alerta] Consumo de Timbres: '.$element['Empresa'];
                            $mail->Body = view('mails.TimbradoAlertMail')->with('data',$data);

                            if(!$mail->send()) {
                                throw new \Exception('Mailer Error: ' . $mail->ErrorInfo);
                            } else {
                                echo 'El correo ha sido enviado';
                            }
                        }
                    catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                    }
            }   
            else{
                echo 'No llega al limite';
            }       
        }
    }
  
}
