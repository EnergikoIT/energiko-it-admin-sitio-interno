<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use DB;
use Excel;
use Redirect;

use App\Models\Ticket;
use App\Models\Factura;
use App\Models\Articulo;
use App\Models\Compueje;
use App\Models\NotasCredito;
use App\Models\TicketDevolucion;

use App\Models\Intelisis\Venta;
use App\Models\Intelisis\Compra;
use App\Models\Intelisis\AnexoMov;

class reportController extends Controller
{
    public function ticketsFacturados(Request $request) {

        $from_date = $request->from_date . ' 00:00:00';
        $to_date = $request->to_date . ' 23:59:59';
        $store_code = $request->store_code;

        $days = Ticket::whereBetween('T_Fecha', [$from_date, $to_date])
            ->orderBy('Fecha', 'ASC')
            ->get([ DB::raw('DISTINCT(CONVERT(DATE,T_Fecha)) AS Fecha') ]);
        
        $total_venta = $total_facturado = $total_diferencia = 0;

        foreach ($days as $key => $day) {
            $data[$key]['fecha'] = $day->Fecha;
            
            $data[$key]['venta'] = (float)Ticket::whereDate('T_Fecha', $day->Fecha)
                ->where('FolTda_Codigo', $store_code)->sum('T_ImporteTotal');
            
            $data[$key]['devoluciones'] = (float)TicketDevolucion::whereDate('TD_Fecha', $day->Fecha)
                ->where('FolTda_Codigo', $store_code)->sum('TD_TotalImporte');

            $data[$key]['venta_neta'] = $data[$key]['venta'] - $data[$key]['devoluciones'];
            $total_venta += $data[$key]['venta_neta']; 
            
            $data[$key]['facturado'] = (float)Factura::whereDate('F_Fecha', $day->Fecha)
                ->where('FolTda_Codigo', $store_code)->where('F_Cancelada', 0)->sum('F_ImporteTotal');
            
            $data[$key]['notas_credito'] = (float)NotasCredito::whereDate('NC_Fecha', $day->Fecha)
                ->where('FolTda_Codigo', $store_code)->sum('NC_TotalImporte');

            $data[$key]['facturacion_neta'] = $data[$key]['facturado'] - $data[$key]['notas_credito'];
            $total_facturado += $data[$key]['facturacion_neta'];

            $data[$key]['diferencia_ventas'] = $data[$key]['venta_neta'] - $data[$key]['facturacion_neta'];
            $total_diferencia += $data[$key]['diferencia_ventas'];
        }

        //Agregar titulos a las columnas
        $headers = ['Fecha', 'Venta Bruta', 'Devoluciones', 'Venta Neta', 'Facturado', 'Notas de Crédito', 'Facturación Neta', 'Diferencia de Ventas'];
        array_unshift($data, $headers);

        //Agregar acomulativos y diferencias
        $footer = ['', '', '', '', '', '', '', ''];
        array_push($data, $footer);
        $footer = ['', '', '', $total_venta, '', '', $total_facturado, $total_diferencia];
        array_push($data, $footer);
        $footer = ['', '', '', 'Venta Total', '', '', 'Facturado Total', 'Diferencia Total'];
        array_push($data, $footer);
        
        $report_name = "Ajuste de venta Tienda $store_code " . explode(' ', $from_date)[0] . " al " . explode(' ', $to_date)[0];
        
        Excel::create($report_name, function($excel) use($data) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->setDescription('Ventas contra facturaciones dentro de un rango de fecha');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->freezeFirstRowAndColumn();
                $sheet->fromArray($data, null, 'A1', true, false);

                $sheet->cells('A1:H1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                    $cells->setFontWeight('bold');
                });
            });
        })->export('xls');
    }

    public function existencias(Request $request) {
        
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $store_code = $request->store_code;

        $data = DB::select("
            SET ARITHABORT OFF
            SET ANSI_WARNINGS OFF

            DECLARE @DesdeFecha AS DATETIME
            DECLARE @HastaFecha AS DATETIME
            DECLARE @DesdeTienda AS INT
            DECLARE @HastaTienda AS INT

            SET @DesdeFecha = CONVERT(DATETIME,'".$from_date." 00:00:00.000')
            SET @HastaFecha = CONVERT(DATETIME,'".$to_date." 23:59:59.000')
            SET @DesdeTienda = '".$store_code."'
            SET @HastaTienda = '".$store_code."'

            SELECT
                AA.Tda_Codigo,
                T.Tda_Nombre,
                AA.Alm_Codigo,
                AL.Alm_Descripcion,
                AA.Art_Codigo,
                A.Art_Descripcion,
                ISNULL((SELECT TOP (1) CE.CE_ExistenciaU
                        FROM Compueje CE
                        WHERE CE.CE_Fecha < @DesdeFecha AND CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo= AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo ORDER BY CE.CE_Fecha DESC), 0) AS 'Inv Inicial U (1)',
                ISNULL((SELECT SUM(CE_Cantidad)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha BETWEEN @DesdeFecha AND @HastaFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 0 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0) AS 'Entradas U (2)',
                ISNULL((SELECT SUM(CE_Cantidad)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo= TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha BETWEEN @DesdeFecha AND @HastaFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 1 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0) AS 'Salidas U (3)',
                ISNULL((SELECT TOP (1) CE.CE_ExistenciaU
                        FROM Compueje CE
                        WHERE CE.CE_Fecha <= @HastaFecha AND CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo= AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo ORDER BY CE.CE_Fecha DESC), 0) AS 'Inv Final U (4)',
                ISNULL((((ISNULL((SELECT SUM(CE_Importe)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha < @DesdeFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 0 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0)-
                ISNULL((SELECT SUM(CE_Importe)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha < @DesdeFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_Codigo = 3 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0)))/
                (ISNULL((SELECT SUM(CE_Cantidad)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha < @DesdeFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 0 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 1)-
                ISNULL((SELECT SUM(CE_Cantidad)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha < @DesdeFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_Codigo = 3 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0)))*
                ISNULL((SELECT TOP (1) CE.CE_ExistenciaU
                        FROM Compueje CE
                        WHERE CE.CE_Fecha < @DesdeFecha AND CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo= AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo ORDER BY CE.CE_Fecha DESC), 0), 0) AS 'Inv Inicial I (5)',
                ISNULL((SELECT SUM(CE_Importe)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo = TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha BETWEEN @DesdeFecha AND @HastaFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 0 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0) AS 'Entradas I (6)',
                ISNULL((SELECT SUM(CE_Importe)
                        FROM Compueje CE INNER JOIN TiposMovimientoAlm TMA ON CE.TMA_Codigo= TMA.TMA_Codigo
                        WHERE(CE.CE_Fecha BETWEEN @DesdeFecha AND @HastaFecha) AND (CE.FolTda_Codigo BETWEEN @DesdeTienda AND @HastaTienda) AND TMA.TMA_AumentaDisminuye = 1 AND
                        CE.AlmTda_Codigo = AA.Tda_Codigo AND CE.Alm_Codigo = AA.Alm_Codigo AND CE.Art_Codigo = AA.Art_Codigo), 0) AS 'Salidas I (7)'
            FROM ArticulosAlmacen AA
                    INNER JOIN Almacenes AL ON
                            AL.Tda_Codigo= AA.Tda_Codigo AND
                            AL.Alm_Codigo= AA.Alm_Codigo
                    INNER JOIN Tiendas T ON
                            T.Tda_Codigo= AA.Tda_Codigo
                    INNER JOIN Articulos A ON
                            AA.Art_Codigo= A.Art_Codigo
            WHERE AA.Tda_Codigo BETWEEN @DesdeTienda AND @HastaTienda AND AA.Alm_Codigo = 1 AND A.Org_Codigo <> 108
            ORDER BY Tda_Nombre,A.Org_Codigo,AA.Art_Codigo");

        $inv_inicial_u = $entradas_u = $salidas_u = $inv_final_u = $inv_inicial_i = $entradas = $salidas = $inv_final_v = 0; 

        foreach($data as $i => $d) {
            $data[$i] = json_decode(json_encode($d), true);
            //Convertir a valores numericos
            $data[$i]['Inv Inicial U (1)'] = floatval($data[$i]['Inv Inicial U (1)']);
            $data[$i]['Entradas U (2)'] = floatval($data[$i]['Entradas U (2)']);
            $data[$i]['Salidas U (3)'] = floatval($data[$i]['Salidas U (3)']);
            $data[$i]['Inv Final U (4)'] = floatval($data[$i]['Inv Final U (4)']);
            $data[$i]['Inv Inicial I (5)'] = floatval($data[$i]['Inv Inicial I (5)']);
            $data[$i]['Entradas I (6)'] = floatval($data[$i]['Entradas I (6)']);
            $data[$i]['Salidas I (7)'] = floatval($data[$i]['Salidas I (7)']);
            $data[$i]['   '] = '   ';
            //Calculos extra
            $data[$i]['Cantidad'] = $data[$i]['Inv Inicial U (1)'] + $data[$i]['Entradas U (2)'];
            $data[$i]['Importe'] = $data[$i]['Inv Inicial I (5)'] + $data[$i]['Entradas I (6)'];
            $data[$i]['Costo Promedio'] = (!$data[$i]['Cantidad'] || $data[$i]['Cantidad'] == $data[$i]['Importe'])? 0 :  ($data[$i]['Importe']/$data[$i]['Cantidad']);

            $ultima_entrada_compra = Compueje::where('FolTda_Codigo', $request->store_code)->where('FolDoc_Codigo', 3)->where('TMA_Codigo', 1)->where('Art_Codigo', $d->Art_Codigo)->orderBy('FolConsecutivo', 'desc')->first();
            $ultima_entrada_ajuste = Compueje::where('FolTda_Codigo', $request->store_code)->where('FolDoc_Codigo', 3)->where('TMA_Codigo', 5)->where('Art_Codigo', $d->Art_Codigo)->orderBy('FolConsecutivo', 'desc')->first();
            
            $data[$i]['Ultimo precio'] = ($ultima_entrada_compra && $ultima_entrada_compra->CE_CostoUnitario >0) ? $ultima_entrada_compra->CE_CostoUnitario : ($ultima_entrada_ajuste? $ultima_entrada_ajuste->CE_CostoUnitario : 0);

            $data[$i]['Inv Valuado'] = $data[$i]['Ultimo precio'] * $data[$i]['Inv Final U (4)'];
            $data[$i]['Inv Final V'] = $data[$i]['Costo Promedio'] * $data[$i]['Inv Final U (4)'];
            //Calcular totales
            $inv_inicial_u += $data[$i]['Inv Inicial U (1)'];
            $entradas_u += $data[$i]['Entradas U (2)'];
            $salidas_u += $data[$i]['Salidas U (3)'];
            $inv_final_u += $data[$i]['Inv Final U (4)'];
            $inv_inicial_i += $data[$i]['Inv Inicial I (5)'];
            $entradas += $data[$i]['Entradas I (6)'];
            $salidas += $data[$i]['Salidas I (7)'];
            $inv_final_v += $data[$i]['Inv Final V']; 
        }
        //dd($data);
        $i += 3; //Ultimo renglon del documento
        
        $footer = ['', '', '', '', '', '', $inv_inicial_u, $entradas_u, $salidas_u, $inv_final_u, $inv_inicial_i, $entradas, $salidas, '', '', '', '', $inv_final_v];
        array_push($data, $footer);

        $report_name = "Existencias tienda $store_code " . explode(' ', $from_date)[0] . " al " . explode(' ', $to_date)[0];

        Excel::create($report_name, function($excel) use($data, $i) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->setDescription('Ventas contra facturaciones dentro de un rango de fecha');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data, $i) {
                $sheet->freezeFirstRow();
                $sheet->fromArray($data, null, 'A1', true, true);

                $sheet->cells('A1:M1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                });
                $sheet->cells('O1:T1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                });
                $sheet->cells('A'.$i.':T'.$i.'', function($cells) use($i) {
                   $cells->setFontWeight('bold');
                   $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function diarioDeMovimientos(Request $request) {

        $store_code = $request->store_code;
        $from_date = $request->from_date . ' 00:00:00';
        $to_date = $request->to_date . ' 23:59:59';

        $codes = [1,2,3,4,15,16,18,19,20,21,101,102,103,105,106,107,108,109,110,111,112,113,115];
        $tma = DB::table('TiposMovimientoAlm')->whereIn('TMA_Codigo', $codes)->get();

        foreach($tma as $key => $movimiento) {
            $data[$key]['codigo'] = $movimiento->TMA_Codigo;
            $data[$key]['movimiento'] = $movimiento->TMA_Descripcion;

            $data[$key]['unidades'] = DB::table('Compueje')
                ->whereBetween('CE_Fecha', [$from_date, $to_date])
                ->where('TMA_Codigo', $movimiento->TMA_Codigo)
                ->where('AlmTda_Codigo', $store_code)
                ->sum('CE_Cantidad');

            $data[$key]['importe'] = DB::table('Compueje')
                ->whereBetween('CE_Fecha', [$from_date, $to_date])
                ->where('TMA_Codigo', $movimiento->TMA_Codigo)
                ->where('AlmTda_Codigo', $store_code)
                ->sum('CE_Importe');
        } 
        
        //Agregar titulos a las columnas
        $headers = ['Código', 'Movimiento', 'Unidades', 'Importe'];
        array_unshift($data, $headers);
        
        $report_name = "Diario de movimientos T$store_code " . explode(' ', $from_date)[0] . " al " . explode(' ', $to_date)[0];
        
        Excel::create($report_name, function($excel) use($data) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->setDescription('Diario de movimientos por almacen');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->freezeFirstRowAndColumn();
                $sheet->fromArray($data, null, 'A1', true, false);

                $sheet->cells('A1:D1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                    $cells->setFontWeight('bold');
                });

            });
        })->export('xls');
    }


     public function facturasSinAfectar(Request $request) {

        $store_code = $request->store_code;
        $from_date = $request->from_date;
        $to_date = $request->to_date;

        $facturas =  DB::connection('intelisis')
            ->select("select VI.ID, VI.Referencia, CONVERT(date, VI.FechaEmision) AS Fecha, VI.Empresa, VI.Almacen, VM.Importe, VM.Impuestos, VM.Importe + VM.Impuestos AS Total, VI.AjusteUCCV
            from [Venta] VI inner join InterfazIC.dbo.Ventas VM on VI.Referencia = VM.FolioVenta
            where VI.Estatus = 'SINAFECTAR' and VI.Referencia like '$store_code-%'");

        $data = [];
        foreach ($facturas as $key => $factura) {            
            $data[$key]['Referencia'] = $factura->Referencia;  
            $data[$key]['Empresa'] = $factura->Empresa; 
            $data[$key]['Almacen'] = $factura->Almacen; 
            $data[$key]['Importe'] = $factura->Importe; 
            $data[$key]['Impuestos'] = $factura->Impuestos; 
            $data[$key]['Total'] = $factura->Total;
            $data[$key]['Fecha'] = $factura->Fecha;

            $factura->mov_intelisis = count(DB::connection('intelisis')->select("select ID from VentaD where ID = '$factura->ID' and Precio is not null"));
            $factura->mov_interfaz = count(DB::connection('interfaz_ic')->select("select VentaID from VentaDetalle where VentaID = '$factura->Referencia'"));

            $data[$key]['Factura_Completa'] = ($factura->mov_intelisis - $factura->mov_interfaz)? '✖' : '✔';
            //$data[$key]['Factura_Completa'] = ($factura->Importe - $factura->AjusteUCCV)? '✖' : '✔';
        }
        //Convertir arreglo de objetos a arreglo de arreglos
        /*
        $facturas = array_map(function ($facturas) {
            return (array)$facturas;
        }, $facturas);
        */
        $report_name = "Facturas sin afectar T$store_code " . explode(' ', $from_date)[0] . " al " . explode(' ', $to_date)[0];
        
        Excel::create($report_name, function($excel) use($data) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->setDescription('Facturas sin afectar en intelisis');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->freezeFirstRowAndColumn();
                $sheet->fromArray($data, null, 'A1', true, true);

                $sheet->cells('A1:H1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                    $cells->setFontWeight('bold');
                });
                $sheet->setColumnFormat(array(
                    'F' => '0.00',
                    'G' => '$#,##0_-'
                ));
            });
        })->export('xls');
    }

    public function anexosMov() {
        $compras = Compra::where('observaciones', 'like', '15-%')->orderBy('FechaEmision', 'desc')->get();
        $data = [];
        
        foreach($compras as $key => $compra) {
            $data[$key]['Folio'] = $compra->Observaciones;
            $data[$key]['FechaEmision'] = substr($compra->FechaEmision, 0, 10);
            $data[$key]['Mov'] = $compra->Mov;
            $data[$key]['Concepto'] = $compra->Concepto;
            $data[$key]['Estatus'] = $compra->Estatus;
            $data[$key]['Almacen'] = $compra->Almacen;
            $data[$key]['PolizaID'] = $compra->PolizaID;
            $data[$key]['Importe'] = $compra->Importe + $compra->Impuestos;
            $data[$key]['Anexo'] = AnexoMov::where('ID', $compra->ID)->where('Rama', 'CXC')->first()? AnexoMov::where('ID', $compra->ID)->where('Nombre', 'like', '%.xml')->first()->Nombre : 'N/A';
        }
        $report_name = "Reporte Anexos Compras";
        
        Excel::create($report_name, function($excel) use($data) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->freezeFirstRowAndColumn();
                $sheet->fromArray($data, null, 'A1', true, true);

                $sheet->cells('A1:I1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                    $cells->setFontWeight('bold');
                });
            });
        })->export('xls');
    }

    public function listaArticulos() {
        $intelisis_articles = DB::connection('intelisis')
            ->table('art')
            ->select(
                'Articulo', 
                'Descripcion1', 
                'Unidad', 
                'Rama', 
                'Impuesto1 as IVA', 
                'Impuesto2 as IEPS', 
                'Proveedor', 
                'Estatus')
            ->take(100)
            ->get();
        
        foreach($intelisis_articles as $key => $article) {
            $article->Precio_cc = Articulo::where('Art_Codigo',$article->Articulo)->first()->Art_UltimoCosto;
            $article->Modificacion_cc = Articulo::where('Art_Codigo',$article->Articulo)->first()->Art_FechaModificacion;

        }
        return $intelisis_articles;
    }


    public function facturasTienda(request $request) {
        
        $store_code = $request->store_code;
        $from_date = $request->from_date . ' 00:00:00';
        $to_date = $request->to_date . ' 23:59:59';

        $invoices = Factura::where('FolTda_codigo', $store_code)->whereBetween('F_Fecha', [$from_date, $to_date])->orderBy('F_Fecha', 'ASC')->get();

        $data = [];
        foreach($invoices as $key => $invoice) {
            $data[$key]['Folio'] = $invoice->FolTda_Codigo .'-'.$invoice->FolEst_Codigo .'-'.$invoice->FolDoc_Codigo .'-'.$invoice->FolConsecutivo;
            $data[$key]['Importe'] = floatval($invoice->F_ImporteTotal);
            $data[$key]['Cliente'] = $invoice->F_Cliente;
            $data[$key]['Cancelada'] = intval($invoice->F_Cancelada)? 'Si': 'No';
            $data[$key]['NotaCredito'] = intval($invoice->F_StatusDevolucion)? 'Si': 'No';
            $data[$key]['Global'] = intval($invoice->F_GlobalDiaria)? 'Si': 'No';
            $data[$key]['Fecha'] = $invoice->F_Fecha;
        }

        $report_name = "Facturas Tienda $store_code";
        
        Excel::create($report_name, function($excel) use($data) {
            $excel->setCreator('arhernandez')->setCompany('Energiko');
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->freezeFirstRowAndColumn();
                $sheet->fromArray($data, null, 'A1', true, true);

                $sheet->cells('A1:G1', function($cells) {
                    $cells->setBackground('#A9CCE3');
                    $cells->setFontWeight('bold');
                });
            });
        })->export('xls');
    }
}
