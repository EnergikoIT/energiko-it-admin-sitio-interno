
@extends('layouts.app')

@section('title', 'Vacantes')

@section('sidebar')
    @parent
@endsection

@section('css')
<style>
</style>
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h2> Editar | Vacante #{{$vacancy->id}} </h2>
        <hr/>
        
        <div class="container">
            <form action="{{url('rh/vacante/actualizar')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="vacancy_id" value="{{$vacancy->id}}">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nombre" value="{{$vacancy->nombre}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="descripcion" rows="4">{{$vacancy->descripcion}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Division</label>
                    <div class="col-sm-10">
                        <!--input type="text" class="form-control" name="division" value="{{$vacancy->division}}"-->
                        <select name="division" id="division" size="1" class="form-control" >
                            <option value="Energia" {{$vacancy->division == 'Energia'? 'selected' : ''}} >Energía</option>
                            <option value="Comercial" {{$vacancy->division == 'Comercial'? 'selected' : ''}}>Comercial</option>
                            <option value="Servicios" {{$vacancy->division == 'Servicios'? 'selected' : ''}}>Servicios</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectlocalidad" class="col-sm-2 col-form-label">Localidad</label>
                    <div class="col-sm-10">
                        <!--input type="text" class="form-control" name="localidad" value="{{$vacancy->localidad}}"-->
                        <select name="localidad" id="selectlocalidad" class="form-control">
                        <option value="Aguascalientes" {{$vacancy->localidad == 'Aguascalientes'? 'selected' : ''}}>Aguascalientes</option>
                        <option value="CDMX" {{$vacancy->localidad == 'CDMX'? 'selected' : ''}}>Ciudad de México</option>
                        <option value="Guanajuato" {{$vacancy->localidad == 'Guanajuato'? 'selected' : ''}}>Guanajuato</option>
                        <option value="Hidalgo" {{$vacancy->localidad == 'Hidalgo'? 'selected' : ''}}>Hidalgo</option>
                        <option value="Jalisco" {{$vacancy->localidad == 'Jalisco'? 'selected' : ''}}>Jalisco</option>
                        <option value="Michoacan" {{$vacancy->localidad == 'Michoacan'? 'selected' : ''}}>Michoacán</option>
                        <option value="Puebla" {{$vacancy->localidad == 'Puebla'? 'selected' : ''}}>Puebla</option>
                        <option value="Sanluis" {{$vacancy->localidad == 'Sanluis'? 'selected' : ''}}>San Luis Potosí</option>
                        <option value="Tlaxcala" {{$vacancy->localidad == 'Tlaxcala'? 'selected' : ''}}>Tlaxcala</option>
                        <option value="Vercruz" {{$vacancy->localidad == 'Vercruz'? 'selected' : ''}}>Veracruz</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo de Puesto</label>
                    <div class="col-sm-10">
                        <!--input type="text" class="form-control" name="tipo_puesto" value="{{$vacancy->tipo_puesto}}"-->
                        <select id="tipo_puesto" name="tipo_puesto" class="form-control">
                            <option value="Almacen" {{$vacancy->tipo_puesto == 'Almacen'? 'selected' : ''}}>Almacén</option>
                            <option value="Administracion" {{$vacancy->tipo_puesto == 'Administracion'? 'selected' : ''}}>Administración</option>
                            <option value="Capitalhumano" {{$vacancy->tipo_puesto == 'Capitalhumano'? 'selected' : ''}}>Capital Humano</option>
                            <option value="Contaduria" {{$vacancy->tipo_puesto == 'Contaduria'? 'selected' : ''}}>Contaduría</option>
                            <option value="Desarrolloorganizacional" {{$vacancy->tipo_puesto == 'Desarrolloorganizacional'? 'selected' : ''}}>Desarrollo Organizacional</option>
                            <option value="Infraestructura" {{$vacancy->tipo_puesto == 'Infraestructura'? 'selected' : ''}}>Infraestructura</option>
                            <option value="Finanzas" {{$vacancy->tipo_puesto == 'Finanzas'? 'selected' : ''}}>Finanzas</option>
                            <option value="Mercadotecnia" {{$vacancy->tipo_puesto == 'Mercadotecnia'? 'selected' : ''}}>Mercadotécnia</option>
                            <option value="Relacionespublicas" {{$vacancy->tipo_puesto == 'Relacionespublicas'? 'selected' : ''}}>Relaciones Públicas</option>
                            <option value="Ventas" {{$vacancy->tipo_puesto == 'Ventas'? 'selected' : ''}}>Ventas</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Empresa</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="empresa" value="{{$vacancy->empresa}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Sexo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="sexo" value="{{$vacancy->sexo}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Edad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="edad" value="{{$vacancy->edad}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Escolaridad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="escolaridad" value="{{$vacancy->escolaridad}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Disponibilidad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="description" name="disponibilidad" value="{{$vacancy->disponibilidad}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo de Vacante</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="description" name="tipo_vacante" value="{{$vacancy->tipo_vacante}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Contratacion</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="description" name="contratacion" value="{{$vacancy->contratacion}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Experiencia</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="description" name="experiencia" value="{{$vacancy->experiencia}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2">Habilitar</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="habilitar" type="checkbox" {{ ($vacancy->habilitar)? 'checked' : ''}}>
                        </label>
                        </div>
                    </div>
                </div>
                
                <fieldset class="form-group row">
                    <div class="col-sm-10">
                        <div class="form-check disabled">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" disabled>
                            Vacante interna (Intranet)
                        </label>
                        </div>
                        <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2" checked>
                            Vacante externa (Pagina Energiko)
                        </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <div class="offset-sm-5 col-sm-1">
                        <button type="button" id='deletebtn' class="btn btn-danger">Eliminar</button>
                    </div>
                    <div class="offset-sm-1 col-sm-1">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmar acción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Estas seguro de que deseas <b>eliminar</b> permanentemente esta vacante? <br>
                    Tambien puedes deshabilitarla para que no este visible publicamente.
                </div>
                <div class="modal-footer">
                    <form action="{{url('rh/vacante/eliminar')}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="vacancy_id" value="{{$vacancy->id}}">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
<script>

$('#deletebtn').on('click', function() {
    $('#myModal').modal('show');
});

</script>
@endsection