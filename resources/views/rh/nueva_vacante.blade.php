
@extends('layouts.app')

@section('title', 'Vacantes')

@section('sidebar')
    @parent
@endsection

@section('css')
<style>

</style>
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h2> Nueva | Vacante </h2>
        <hr/>
        
        <div class="container">
            <form method="post" action="{{url('rh/vacante/guardar')}}">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nombre">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
                    <div class="col-sm-10">
                        <textarea name="descripcion" class="form-control" id="descripcion" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Division</label>
                    <div class="col-sm-10">
                        <select name="division" id="division" size="1" class="form-control" >
                            <option value="energia">Energía</option>
                            <option value="comercial">Comercial</option>
                            <option value="servicios">Servicios</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Localidad</label>
                    <div class="col-sm-10">
                        <select name="localidad" id="selectlocalidad" class="form-control">
                        <option value="aguascalientes">Aguascalientes</option>
                        <option value="cdmx">Ciudad de México</option>
                        <option value="guanajuato">Guanajuato</option>
                        <option value="hidalgo">Hidalgo</option>
                        <option value="jalisco">Jalisco</option>
                        <option value="michoacan">Michoacán</option>
                        <option value="puebla">Puebla</option>
                        <option value="sanluis">San Luis Potosí</option>
                        <option value="tlaxcala">Tlaxcala</option>
                        <option value="vercruz">Veracruz</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo de Puesto</label>
                    <div class="col-sm-10">
                        <select id="pos" name="tipo_puesto" class="form-control">
                            <option value="almacen">Almacén</option>
                            <option value="administracion">Administración</option>
                            <option value="capitalhumano">Capital Humano</option>
                            <option value="contaduria">Contaduría</option>
                            <option value="desarrolloorganizacional">Desarrollo Organizacional</option>
                            <option value="infraestructura">Infraestructura</option>
                            <option value="finanzas">Finanzas</option>
                            <option value="mercadotecnia">Mercadotécnia</option>
                            <option value="relacionespublicas">Relaciones Públicas</option>
                            <option value="ventas">Ventas</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Empresa</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="empresa">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Sexo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="sexo">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Edad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="edad">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Escolaridad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="escolaridad">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Disponibilidad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"  name="disponibilidad">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo de Vacante</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="tipo_vacante">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Contratacion</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="contratacion">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Experiencia</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="experiencia">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2">Habilitar</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" name="habilitar" type="checkbox">
                        </label>
                        </div>
                    </div>
                </div>
                
                <fieldset class="form-group row">
                    <div class="col-sm-10">
                        <div class="form-check disabled">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="radio" value="option1" disabled>
                            Vacante interna (Intranet)
                        </label>
                        </div>
                        <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="radio" value="option2" checked>
                            Vacante externa (Pagina Energiko)
                        </label>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-1">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </main>
@endsection

@section('scripts')
<script>

</script>
@endsection