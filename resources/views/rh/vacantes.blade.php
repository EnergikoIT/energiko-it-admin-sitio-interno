
@extends('layouts.app')

@section('title', 'Vacantes')

@section('sidebar')
    @parent
@endsection

@section('css')
<style>
    .clickable-row { 
        cursor: pointer; 
        cursor: hand; 
    }
</style>
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h2> Vacantes </h2>
    
        <div class="offset-sm-10 col-sm-2">
            <button class="btn btn-info" onclick="location.href='vacante/nueva';">Nueva Vacante <i class="fa fa-plus"></i></button>
        </div>

        <br />
        <section class="table-section">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th> ID </th>
                        <th> Activa </th>
                        <th> Nombre </th>
                        <th> Localidad </th>
                        <th> Empresa </th>
                        <th> Fecha </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vacancys as $vacancy)
                    <tr class='clickable-row' data-href="{{url('rh/vacante/'.$vacancy->id.'/editar')}}">
                        <th> {{$vacancy->id}} </th>
                        <th style="padding-left: 20px;"> <i class="fa fa-{{$vacancy->habilitar? 'check': 'remove'}}" aria-hidden="true"></i> </th>
                        <td> {{$vacancy->nombre}}  </td>
                        <td> {{$vacancy->localidad}}  </td>
                        <td> {{$vacancy->empresa}}  </td>
                        <td> {{ substr($vacancy->created_at, 0, 10) }}  </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
    </main>
@endsection

@section('scripts')
<script>
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            location.replace($(this).data("href"));
        });
    });
</script>
@endsection