<!DOCTYPE html>
<html>
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Dashboard para administrar reportes">
		<meta name="author" content="Departamento de aplicaciones">
		<link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">

      <title>@yield('title') | Energiko </title>
      
      <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
      <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
      <script src="{{asset('js/bootstrap.js')}}" type="text/javascript"></script>
      
      <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
      <link href="{{asset('css/monthly.css')}}" rel="stylesheet" >
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
      
      <style> 
        #chartdiv {
          width		: 100%;
          height		: 500px;
          font-size	: 11px;
        }		

        * {
          margin: 0;
          padding: 0;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }

        ul { list-style-type: none; }

        .accordion {
          width: 100%;
          max-width: 360px;
          margin: 0px auto 20px;
          background: #FFF;
          -webkit-border-radius: 4px;
          -moz-border-radius: 4px;
          border-radius: 4px;
        }

        .accordion .link {
          cursor: pointer;
          display: block;
          padding: 15px 15px 15px 42px;
          color: #4D4D4D;
          font-size: 14px;
          font-weight: 700;
          border-bottom: 1px solid #CCC;
          position: relative;
          -webkit-transition: all 0.4s ease;
          -o-transition: all 0.4s ease;
          transition: all 0.4s ease;
        }

        .accordion li:last-child .link { border-bottom: 0; }

        .accordion li i {
          position: absolute;
          top: 16px;
          left: 12px;
          font-size: 18px;
          color: #595959;
          -webkit-transition: all 0.4s ease;
          -o-transition: all 0.4s ease;
          transition: all 0.4s ease;
        }

        .accordion li i.fa-chevron-down {
          right: 12px;
          left: auto;
          font-size: 16px;
        }

        .accordion li.open .link { color: #009926; }

        .accordion li.open i { color: #009926; }

        .accordion li.open i.fa-chevron-down {
          -webkit-transform: rotate(180deg);
          -ms-transform: rotate(180deg);
          -o-transform: rotate(180deg);
          transform: rotate(180deg);
        }

        .accordion li.selected .link { color: #009926; }

        .accordion li.selected i { color: #009926; }

        .navbar {
          border-bottom: 4px solid green;
        }

        .submenu {
          display: none;
          background: #292b2c;
          font-size: 14px;
        }

        .submenu li { border-bottom: 1px solid #4b4a5e; }

        .submenu a {
          display: block;
          text-decoration: none;
          color: #d9d9d9;
          padding: 12px;
          padding-left: 42px;
          -webkit-transition: all 0.25s ease;
          -o-transition: all 0.25s ease;
          transition: all 0.25s ease;
        }

        .submenu a:hover {
          background: #4CAF50;
          color: #FFF;
        }

        .sec-text {
          margin-top: 14px;
          margin-left: -8px;
          color: #b3b3b3;
          letter-spacing: 1px;
          font-size: 14px;
          font-weight: 500;
          font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

      </style>
      @yield('css')
    </head>
    <body>
        @section('sidebar')
        <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
          <button class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="{{url('home')}}"><img src="{{asset('img/logo-energiko.png')}}" width="140px;"/> </a> 
          <!--small class="sec-text">Intranet</small-->

          <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
            </ul>
            <form class="form-inline mt-2 mt-md-0">
              <label style="color: lightgray;">Bienvenido, Happy Go!</label> 
              &nbsp; &nbsp; 
              <img class="rounded-circle" width="34" src="{{asset('img/iconHG.jpg')}}" alt="avatar"/>
            </form>
          </div>
        </nav>
        @show
        <div class="container-fluid">
          <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
            <ul id="accordion" class="accordion">
              <li class="{{(Route::current()->uri == 'home')? 'selected': ''}}">
                <div class="link hm" onclick="window.location.href = '{{url('home')}}'"><i class="fa fa-home"></i>Inicio</div>
              </li>
              <li class="{{ (Route::current()->action['prefix'] == '/contabilidad')? 'selected' : '' }}">
                <div class="link co"><i class="fa fa-book"></i>Contabilidad<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                  <li><a class="nav-link" href="{{url('contabilidad/tiendas')}}">Tablero</a></li>
                  <li><a class="nav-link" href="{{url('contabilidad/cortesz')}}"> Cortes Z </a></li>
                  <!--<li><a class="nav-link" href="{{url('contabilidad/analisis')}}">Estadisticas</a></li>-->
                  <li><a class="nav-link" href="{{url('contabilidad/reportes')}}"> Reportes </a></li>
                </ul>
              </li>
              <li class="{{ (Route::current()->action['prefix'] == '/herramientas')? 'selected' : '' }}">
                <div class="link co"><i class="fa fa-cog"></i>Herramientas<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                  <li><a class="nav-link" href="{{url('herramientas/compras/nasoc')}}">Control de compras</a></li>
                </ul>
              </li>
              <li class="{{ (Route::current()->action['prefix'] == '/it')? 'selected' : '' }}">
                <div class="link it"><i class="fa fa-code"></i>TI<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                  <li><a target="_blank" href="http://10.1.100.129/zabbix/dashboard.php">Zabbix</a></li>
                  <li><a target="_blank" href="https://mesa-ayuda.energiko.com/helpdesk/WebObjects/Helpdesk.woa">Mesa de ayuda</a></li>
                  <li><a target="_blank" href="http://energiko.net/portal_gastos/web/login.aspx">Portal de gastos</a></li>
                </ul>
              </li>

              <!--
              <li class="{{ (Route::current()->action['prefix'] == '/rh')? 'selected' : '' }}">
                <div class="link rh"><i class="fa fa-users"></i>Recursos Humanos<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                  <li><a class="nav-link {{(Route::current()->uri == 'rh/vacantes')? 'active': ''}}" href="{{url('rh/vacantes')}}">Vacantes</a></li>
                </ul>
              </li>
              -->
              <li class="{{ (Route::current()->action['prefix'] == '/dir')? 'selected' : '' }}">
                <div class="link dir"><i class="fa fa-address-book"></i>Directorio<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                  <li><a target="_blank" href="http://10.50.1.236/directorio">Disa</a></li>
                  <li><a target="_blank" href="http://10.1.101.163/directorio/">Concentro</a></li>
                  <li><a target="_blank" href="https://10.1.33.3/directorio/">Celtis</a></li>
                </ul>
              </li>
            </ul>

          </nav>
          @yield('content')
      </div>
      <script>

        $(function() {
          var Accordion = function(el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            var links = this.el.find('.link');            
            var mod;

            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)

            if(window.location.href.indexOf("/contabilidad") > -1) {
              mod = 'co';  
            } else if(window.location.href.indexOf("/rh") > -1) {
              mod = 'rh';
            }
            //var active_module = this.el.find('.link.' + mod);
            //active_module.trigger("click");
          }

          Accordion.prototype.dropdown = function(e) {
            var $el = e.data.el;
            $this = $(this),
            $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
              $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
              $el.find('.submenu').not($next).slideUp().parent().removeClass('selected');
              $(".link.hm").parent().removeClass("selected");
            };
          }  

          var accordion = new Accordion($('#accordion'), false);
        });
      </script>
      @yield('scripts')
    </body>
</html>