<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Información sobre el consumo de <b>Timbres</b>: </p>
    <table>
        <tr style="background-color: #96ad3a;">
            <th>Empresa</th>
            <th>Timbres Contratados</th> 
            <th>Timbres Usados</th>
            <th>Timbres Restantes</th>                    
        </tr>
            <tr style="background-color: #dddddd;">
                 <td>{{$data['empresa']}}</td>
                <td>{{$data['contratados']}}</td>
                <td>{{$data['usados']}}</td>
                <td>{{$data['restantes']}}</td>               
            </tr> 
    </table>
    <p>
        <b>Si se ha alcanzado el limite mínimo de timbres, por favor de revisar con su Proveedor PAC la contratación de nuevos timbres. <b><br>        
    </p>
</body>
</html>
