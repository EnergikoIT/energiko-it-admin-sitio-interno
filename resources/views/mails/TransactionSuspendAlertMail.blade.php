<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que <b>existen transacciones suspendidas:</b></p>
    <table>
        <tr style="background-color: #96ad3a;">
            <th>Agente</th>
            <th>No de Operacion</th>
            <th>Fecha</th>
        </tr style="background-color: #dddddd;">
        @foreach($data as $key =>$transaction)
            <tr>
                <td>{{$transaction->Agente}}</td>
                <td>{{$transaction->OperNbr}}</td>
                <td>{{$transaction->Fecha}}</td>
            </tr> 
         @endforeach           
    </table>
    <p>
        <b>Por favor de validar las transacciones suspendidas que estan deteniendo el procesamiento<b><br>        
    </p>
</body>
</html>