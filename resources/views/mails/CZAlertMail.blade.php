<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que <b>NO</b> se ha generado un corte Z para la tienda <b> {{$corte->tienda}} </b> </p>
    <p>Este error debera de ser corregido a la brevedad para no ocasionar problemas administrativos, las recomendaciones para solucionar el problema son las siguientes: </p>
    <ul>
        <li>Dejar encendidas todas las maquinas en donde se tenga el punto de venta instalado</li>
        <li>Dejar abierto el punto de venta y con el usuario ingresado</li>
    </ul>
    <table>
    <tr>
        <th>Tienda</th>
        <th>Estación</th>
        <th>Último Ticket</th>
        <th>Último CorteZ</th>
    </tr>
    <tr style="background-color: #ffe1e1;">
        <td>{{$corte->tienda}}</td>
        <td>{{$corte->Est_Codigo}}</td>
        <td>{{$corte->MAX_T}}</td>
        <td>{{$corte->MAX_CZ}}</td>
    </tr>
    </table>
    <p><b>Si este error persiste despues de seguir estas recomendaciones favor de ponerse en contacto con el area de IT de Energiko.<b></p>
</body>
</html>
