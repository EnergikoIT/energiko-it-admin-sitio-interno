<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body> 
    <p>Esta es una alerta automática para informar que <b>existen movimientos duplicados </b> en el Administrador de Procesos: <b> </b> </p>
    <table>
        <tr style="background-color: #96ad3a;">        
            <th>Número Cedi</th>
            <th>Nombre Cedi</th>
            <th>Repetido</th> 
            <th>Orden de Venta</th>                  
            <th>Embarque</th> 
            <th>Orden de Compra</th>
            <th>Tipo Proceso</th>  
            <th>Prioridad</th> 
            <th>Pantalla</th>  
            
        </tr style="background-color: #dddddd;">
        @foreach($data as $key => $index)
        <tr>
            <th scope="row"> {{$index->Cedi}} </th>    
            <td> {{$index->NombreCedi}}  </td>  
            <td> {{$index->Movimientos}}  </td>     
            <td> {{$index->OrdenVenta}}  </td>  
            <td> {{$index->Embarque}}  </td> 
            <td> {{$index->OrdenCompra}}  </td>
            <td> {{$index->TipoProceso}}  </td>      
            <td> {{$index->Prioridad}}  </td>    
            <td> {{$index->Pantalla}}  </td>  
        </tr>
        @endforeach             
    </table>
    <p>
        <b>Grupo Energiko. Área de IT. Departamento de Aplicaciones<b><br>        
    </p>
</body>
</html>