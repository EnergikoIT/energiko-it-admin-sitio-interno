<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body> CpnyId,Crtd_DateTime,ShipperID,MessageText
    <p>Esta es una alerta automática para informar que <b>se presento un error </b> en el Administrador de Procesos: <b> </b> </p>
    <table>
        <tr style="background-color: #96ad3a;">        
            <th>Cedi</th>         
            <th>Embarque</th> 
            <th>Mensaje de error</th>
            <th>Fecha</th>    
        </tr style="background-color: #dddddd;">
        @foreach($data as $key => $index)
        <tr>
            <th scope="row"> {{$index->CpnyId}} </th>    
            <td> {{$index->ShipperID}}  </td>     
            <td> {{$index->MessageText}}  </td>  
            <td> {{$index->Crtd_DateTime}}  </td>      
        </tr>
        @endforeach             
    </table>
    <p>
        <b>Por favor de validar ingresar a la sesión cerrar el error y volver a ejecutar el programa.<b><br>        
    </p>
</body>
</html>