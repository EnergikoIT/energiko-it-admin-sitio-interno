<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que el <b>Administrador de Procesos</b> no esta abierto o se encuentra bloqueado<b> </b> </p>
    
    <p>
        <b>Por favor de ingresar a la sesión si se encuentra abierto cerrarlo y volverlo a ejecutar.<b><br>        
    </p>
</body>
</html>