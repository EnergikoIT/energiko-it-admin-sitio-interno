<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que <b>existe retraso</b> en el procesamiento de transacciones por parte de los siguientes Agentes : <b> </b> </p>
    <table>
        <tr style="background-color: #96ad3a;">
            <th>Agente</th>
            <th>Transacciones</th>
        </tr>
        @foreach($dato as $key =>$agente)
         @foreach($agente as $element)
            <tr style="background-color: #dddddd;">
                <td>{{$element->Agente}}</td>
                <td>{{$element->Transacciones}}</td>
            </tr> 
         @endforeach
        @endforeach       
    </table>
    <p>
        <b>Por favor de validar que no exista errores de procesamiento, perdida de conexión o bloqueo constante en la Base de Datos<b><br>        
    </p>
</body>
</html>
