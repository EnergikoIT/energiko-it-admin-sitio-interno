<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que <b>existen más de 600 transacciones pendientes</b> en el Administrador de Procesos : <b> </b> </p>
    <table>
        <tr style="background-color: #96ad3a;">        
            <th>Cedi</th>
            <th>Número de transacciones pendientes</th>   
            <th>Fecha/Hora última transacción</th>  
        </tr style="background-color: #dddddd;">
        @foreach($data as $key => $index)
        <tr>
            <th scope="row"> {{$index->CpnyId}} </th>
            <td> {{$index->Transacciones}}  </td>
            <td> {{$index->FechaAntigua}}  </td>
        </tr>
        @endforeach             
    </table>
    <p>
        <b>Por favor de validar que no exista errores de procesamiento, perdida de conexión o bloqueo constante en la Base de Datos<b><br>        
    </p>
</body>
</html>