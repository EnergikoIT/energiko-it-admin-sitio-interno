<!DOCTYPE html>
<html>
<head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
</head>
<body>
    <p>Esta es una alerta automática para informar que <b>no</b> se ha timbrado una o mas facturas en la tienda <b> {{App\Models\Tienda::where('Tda_Codigo', $store_invoices[0]->FolTda_Codigo)->first()->Tda_Nombre}} </b> </p>
    <table>
        <tr>
            <th>Folio</th>
            <th>Importe</th>
            <th>Fecha</th>
        </tr>
        @foreach($store_invoices as $key => $invoice)
        <tr style="background-color: {{!($key%2)? '#e6e6e6': ''}}">
            <td>{{$invoice->FolTda_Codigo . '-' . $invoice->FolEst_Codigo . '-' . $invoice->FolDoc_Codigo . '-' . $invoice->FolConsecutivo}}</td>
            <td>{{money_format('%.2n', $invoice->F_ImporteTotal)}}</td>
            <td>{{substr($invoice->F_Fecha, 0, 10)}}</td>
        </tr>
        @endforeach
    </table>
    <p>
        <b>Si existen dudas en el proceso de timbrado consulte el siguiente manual:<b><br>
        <a href="https://drive.google.com/file/d/0B1ThPsunV_cla3FNcmpWMzZGM1E/view?usp=sharing"> Timbrado de facturas (PDF)</a>
    </p>
</body>
</html>
