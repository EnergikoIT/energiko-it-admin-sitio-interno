
@extends('layouts.app')

@section('title', 'Reportes')

@section('css')

@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h1>Reportes</h1>
        <form id="live_form" method="post" action="{{url('contabilidad/reportes/vcf')}}">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <legend>Reportes Compucaja: </legend>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="report" data-url="{{url('contabilidad/reportes/vcf')}}" value="option1" checked>
                        Ventas contra facturaciones por periodo
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="report" data-url="{{url('contabilidad/reportes/existencias')}}" value="option2">
                        Reporte de existencias
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="report" data-url="{{url('contabilidad/reportes/ddm')}}" value="option3">
                        Diario de movimientos
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="report" data-url="{{url('contabilidad/reportes/facturas')}}" value="option5">
                        Facturas por periodo
                    </label>
                </div>

                <hr />

                <legend>Reportes Intelisis: </legend>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="report" data-url="{{url('contabilidad/reportes/fsa')}}" value="option4">
                        Facturas sin afectar
                    </label>
                </div>
            </fieldset>

            <div class="form-group" id='date-range'>
                <div class="row">
                    <div class="col-6">
                        <label> Fecha inicial (aaaa/mm/dd): </label>
                        <input type="text" name="from_date" placeholder="aaaa-mm-dd" value="{{$from_date}}" class="form-control" onkeyup="var date = this.value;if (date.match(/^\d{4}$/) !== null) {this.value = date + '-';} else if (date.match(/^\d{4}\-\d{2}$/) !== null) {this.value = date + '-';}" maxlength="10">
                    </div>
                    <div class="col-6">
                        <label> Fecha final: </label>
                        <input type="text" name="to_date" placeholder="aaaa-mm-dd" value="{{$to_date}}" class="form-control" onkeyup="var date = this.value;if (date.match(/^\d{4}$/) !== null) {this.value = date + '-';} else if (date.match(/^\d{4}\-\d{2}$/) !== null) {this.value = date + '-';}" maxlength="10">
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="stores">
                <label for="store_code">Selecciona la tienda</label>
                <select class="form-control" id="store_code" name="store_code">
                    @foreach($stores as $tienda)
                    <option value="{{$tienda->Tda_Codigo}}">{{$tienda->Tda_Codigo . ' - ' . $tienda->Tda_Nombre}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group text-center" id="download_btn">
                <button type="submit" class="btn btn-primary">Descargar <i class="fa fa-download" aria-hidden="true"></i></button>
            </div>
        </form>	
    </main>
    
@endsection

@section('scripts')
    <script>
        $(document).ready(function() { 

            var testimonial_ok=false;
            
            var report = $('#live_form input:radio[name=report]');			
            
            var date_range = $('#live_form #date-range');
            var download = $('#live_form #download_btn');
            var stores = $('#live_form #stores');
            var all = date_range.add(download).add(stores); 
            
            report.change(function() {
                var value=this.value;
                all.addClass('hidden-xs-up');					
                
                if (value == 'option1'){
                    date_range.removeClass('hidden-xs-up');
                    download.removeClass('hidden-xs-up');	
                    stores.removeClass('hidden-xs-up');

                    $('#live_form').attr('action', report.data('url') );						
                }
                else if (value == 'option2'){
                    date_range.removeClass('hidden-xs-up');
                    download.removeClass('hidden-xs-up');	
                    stores.removeClass('hidden-xs-up');
 
                    $('#live_form').attr('action', 'reportes/existencias' );	
                }		
                else if (value == 'option3'){
                    date_range.removeClass('hidden-xs-up');
                    download.removeClass('hidden-xs-up');	
                    stores.removeClass('hidden-xs-up');
 
                    $('#live_form').attr('action', 'reportes/ddm' );	
                }
                else if (value == 'option4'){
                    download.removeClass('hidden-xs-up');	
                    stores.removeClass('hidden-xs-up');
 
                    $('#live_form').attr('action', 'reportes/fsa' );	
                }
                else if (value == 'option5'){
                    date_range.removeClass('hidden-xs-up');
                    download.removeClass('hidden-xs-up');	
                    stores.removeClass('hidden-xs-up');
 
                    $('#live_form').attr('action', 'reportes/facturas' );	
                }
            });	
        });
    </script>
@endsection