
@extends('layouts.app')

@section('title', 'Estatus tiendas')

@section('sidebar')
    @parent
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h2> Ultimos cortes Z</h2>
        <br/>
        <section class="table-section">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>CC</th>
                        <th> Tienda </th>
                        <th>Estación</th>
                        <th>Ultimo ticket</th>
                        <th>Ultimo Corte Z</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $corte)
                    <tr class="{{ ($corte->MAX_CZ < $corte->MAX_T ) && ($corte->MAX_CZ !== date('Y-m-d', strtotime('yesterday'))  ) ? 'table-danger' : '' }}">
                        <th scope="row"> {{$corte->Tda_Codigo}} </th>
                        <td> {{$corte->nombre}}  </td>
                        <th> {{$corte->Est_Codigo}} </th>
                        <td> {{$corte->MAX_T}} </td>
                        <td> {{$corte->MAX_CZ}}  </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
    </main>
@endsection