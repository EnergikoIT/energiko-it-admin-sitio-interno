
@extends('layouts.app')

@section('title', 'Tablero tiendas')

@section('sidebar')
    @parent
@endsection

@section('content')
    <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
        <h2>Tablero de tiendas</h2>
        <br/>
        <section class="table-section">
            <form>
                <div class="row">
                    <div class="col-4">
                        <label> Fecha inicial: </label>
                        <input type="text" name="from_date" placeholder="aaaa-mm-dd" value="{{$from_date}}" class="form-control" onkeyup="var date = this.value;if (date.match(/^\d{4}$/) !== null) {this.value = date + '-';} else if (date.match(/^\d{4}\-\d{2}$/) !== null) {this.value = date + '-';}" maxlength="10">
                    </div>
                    <div class="col-4">
                        <label> Fecha final: </label>
                        <input type="text" name="to_date" placeholder="aaaa-mm-dd" value="{{$to_date}}" class="form-control" onkeyup="var date = this.value;if (date.match(/^\d{4}$/) !== null) {this.value = date + '-';} else if (date.match(/^\d{4}\-\d{2}$/) !== null) {this.value = date + '-';}" maxlength="10">
                    </div>
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary" style="margin-top: 32px;">Actualizar</button>
                    </div>
                </div>
            </form>
            <br/>
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>CC</th>
                        <th>Nombre</th>
                        <th>Vendido (+)</th>
                        <th>Facturado (-)</th>
                        <th>Diferencia (=)</th>
                        <th>F. Timbradas</th>
                        <th>T. Sincro</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $store)
                    <tr class="store {{ $store['diferencia_ventas']? 'table-danger' : '' }}" data-store="{{$key}}">
                        <th scope="row"> {{$key}} </th>
                        <td> {{ $store['nombre'] }} </td>
                        <td> {{  (function_exists("money_format"))?  money_format('%.2n', $store['venta_neta']) : $store['venta_neta'] }}</td>
                        <td> {{ (function_exists("money_format"))?  money_format('%.2n', $store['facturacion_neta']) : $store['facturacion_neta'] }}</td>
                        <td> {{ (function_exists("money_format"))?  money_format('%.2n', $store['diferencia_ventas']) : $store['diferencia_ventas'] }}</td>
                        <td>
                        @if(!$store['timbrado']) 
                            <i style="color: #2db92d;margin-left: 20%;" class="fa fa-check-circle" aria-hidden="true"></i>
                        @else
                            <i style="color: #b22222;margin-left: 20%;" class="fa fa-times-circle" aria-hidden="true"></i>
                        @endif
                        </td>
                        <td> 
                         @if($store['sincro'] == date('Y-m-d')) 
                            <i style="color: #2db92d;margin-left: 20%;" class="fa fa-check-circle" aria-hidden="true"></i>
                        @else
                            <i style="color: #b22222;margin-left: 20%;" class="fa fa-times-circle" aria-hidden="true"></i>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Timbrado de facturas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pending">
                        <p>Las facturas mostradas a continuación no han sido timbradas: </p>
                        <table class="table">
                            <thead class="thead-default">
                                <tr>
                                    <th>Folio</th>
                                    <th>Importe</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody id='pending_invoices'>
                                <!--a> Esta tabla se llena por Ajax <a-->
                            </tbody>
                        </table>
                    </div>
                    <div class="no-pending" style="text-align: center;">
                        <h5> No hay facturas pendientes por timbrar </h5>
                        <i style="color: #2db92d;" class="fa fa-check-circle fa-4x" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        
    </main>
@endsection

@section('scripts')
<script>

$('.store').on('click', function() {
    $('#myModal').modal('show');
    
    store = $(this).data("store");

    $.ajax({
        url: "../ajax/facturaspendientes/" + store,
        context: document.body,
        dataType: 'json',
        success: function(data) {
            if(data.length) {
                $('.pending').css( { "display" : "inline" });
                $('.no-pending').css( { "display" : "none" });
                var html = '';
                $.each(data, function (key, data) {
                    html += '<tr>';
                    html +=     '<td>'+data.Folio+'</td>';
                    html +=     '<td>$'+data.Importe+'</td>';
                    html +=     '<td>'+data.Fecha+'</td>';
                    html += '</tr>';
                });
                $('#pending_invoices').html(html);
            } else {
                $('.pending').css( { "display" : "none" });
                $('.no-pending').css( { "display" : "block" });
            }
        }
    });
});

</script>
@endsection