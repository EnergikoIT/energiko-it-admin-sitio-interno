
@extends('layouts.app')

@section('title', 'Inicio')

@section('sidebar')
    @parent    
@endsection

@section('css')
<style>

@font-face {
    font-family: 'weather';
    src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot');
    src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot?#iefix') format('embedded-opentype'),
         url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.woff') format('woff'),
         url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.ttf') format('truetype'),
         url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.svg#artill_clean_weather_iconsRg') format('svg');
    font-weight: normal;
    font-style: normal;
}

#weather {
  margin: 0px auto;
  text-align: center;
  text-transform: uppercase;
  background: #1192d3;
  background-size: cover;
}


i {
  color: #fff;
  font-family: weather;
  font-size: 150px;
  font-weight: normal;
  font-style: normal;
  line-height: 1.0;
  text-transform: none;
}

.icon-0:before { content: ":"; }
.icon-1:before { content: "p"; }
.icon-2:before { content: "S"; }
.icon-3:before { content: "Q"; }
.icon-4:before { content: "S"; }
.icon-5:before { content: "W"; }
.icon-6:before { content: "W"; }
.icon-7:before { content: "W"; }
.icon-8:before { content: "W"; }
.icon-9:before { content: "I"; }
.icon-10:before { content: "W"; }
.icon-11:before { content: "I"; }
.icon-12:before { content: "I"; }
.icon-13:before { content: "I"; }
.icon-14:before { content: "I"; }
.icon-15:before { content: "W"; }
.icon-16:before { content: "I"; }
.icon-17:before { content: "W"; }
.icon-18:before { content: "U"; }
.icon-19:before { content: "Z"; }
.icon-20:before { content: "Z"; }
.icon-21:before { content: "Z"; }
.icon-22:before { content: "Z"; }
.icon-23:before { content: "Z"; }
.icon-24:before { content: "E"; }
.icon-25:before { content: "E"; }
.icon-26:before { content: "3"; }
.icon-27:before { content: "a"; }
.icon-28:before { content: "A"; }
.icon-29:before { content: "a"; }
.icon-30:before { content: "A"; }
.icon-31:before { content: "6"; }
.icon-32:before { content: "1"; }
.icon-33:before { content: "6"; }
.icon-34:before { content: "1"; }
.icon-35:before { content: "W"; }
.icon-36:before { content: "1"; }
.icon-37:before { content: "S"; }
.icon-38:before { content: "S"; }
.icon-39:before { content: "S"; }
.icon-40:before { content: "M"; }
.icon-41:before { content: "W"; }
.icon-42:before { content: "I"; }
.icon-43:before { content: "W"; }
.icon-44:before { content: "a"; }
.icon-45:before { content: "S"; }
.icon-46:before { content: "U"; }
.icon-47:before { content: "S"; }

#weather h2 {
  margin: 0 0 8px;
  color: #fff;
  font-size: 60px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather ul {
  margin: 0;
  padding: 0;
}

#weather li {
  background: rgba(255,255,255,0.70);
  padding: 5px;
}

.avatar {
  height: 80px;
  width: 80px;
}

#map {
  height: 520px;
}

</style>
@endsection

@section('content')
    
  <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
    
    <div class="container-fluid">
    <!--
      <div class="row"> 
        <div class="card col-10">
          <div class="card-header">
            Destacado
          </div>
          <div class="card-block">
            <div class="media">
              <img class="d-flex mr-3 avatar" src="https://www.shareicon.net/data/128x128/2016/05/29/772560_user_512x512.png" alt="Generic placeholder image">
              <div class="media-body">
                <h5 class="mt-0">Capital Humano</h5>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit velit ipsum, ut consequat nunc condimentum sed.
                <div class="media mt-3">
                  <a class="d-flex pr-3" href="#">
                    <img class="avatar" src="https://image.flaticon.com/icons/png/128/149/149072.png" alt="Generic placeholder image">
                  </a>
                  <div class="media-body">
                    <h5 class="mt-0">Departamento de Aplicaciones</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-muted">
            <a href="#">Ver más</a>
          </div>
        </div>
        <div class="col-2">
          <div id="weather"></div>
        </div>
      </div>
      -->
      <br />

      <div class="row">
        <div class="col-6">
          <div id="map"></div>
          <script>
            function initMap() {
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: {lat: 20.7103889, lng: -103.40984739999999}
              });

              var trafficLayer = new google.maps.TrafficLayer();
              trafficLayer.setMap(map);

              var marker = new google.maps.Marker({
                position: {lat: 20.7103889, lng: -103.40984739999999},
                map: map,
                animation: google.maps.Animation.DROP,
                title: 'Torre Celtis'
              });
            }
          </script>
          <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwCYZVLkVHooJmj772Az7Q0P0dJHkjJ4o&callback=initMap">
          </script>
        </div>
        <div class="col-6">
          <div class="monthly" id="mycalendar"></div>
        </div>
      </div>
    </div>

  </main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('js/monthly.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
<script type="text/javascript">
    var sampleEvents = {
      "monthly": [
        {
        "id": 1,
        "name": "Cumpleaños Roberto Hernandez",
        "startdate": "2017-04-12",
        "enddate": "2017-04-12",
        "starttime": "12:00",
        "endtime": "1:00",
        "color": "#99CCCC",
        "url": ""
        },
        {
        "id": 2,
        "name": "Puente",
        "startdate": "2017-04-13",
        "enddate": "2017-04-14",
        "starttime": "1:00",
        "endtime": "12:00",
        "color": "#CC99CC",
        "url": ""
        },
        {
        "id": 3,
        "name": "Cumple Elizabeth Monreal",
        "startdate": "2017-04-15",
        "enddate": "2017-04-15",
        "starttime": "",
        "endtime": "",
        "color": "#666699",
        "url": "https://www.google.com/"
        },
        {
        "id": 4,
        "name": "Evento de prueba",
        "startdate": "2017-04-15",
        "enddate": "2017-04-15",
        "starttime": "",
        "endtime": "",
        "color": "#666699",
        "url": "https://www.google.com/"
        },
        {
        "id": 8,
        "name": "Dia del niño",
        "startdate": "2017-04-30",
        "enddate": "2017-04-30",
        "starttime": "",
        "endtime": "",
        "color": "#666699",
        "url": "https://www.google.com/"
        }
      ]
    };

	$(window).load( function() {
		$('#mycalendar').monthly({
			mode: 'event',
			dataType: 'json',
			events: sampleEvents
		});
	});

  /* Clima
  $(document).ready(function() {
    $.simpleWeather({
      location: 'Guadalajara, JA',
      woeid: '',
      unit: 'c',
      success: function(weather) {
        html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
        html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
        html += '<li class="currently">'+weather.currently+'</li>';
        html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</li></ul>';
    
        $("#weather").html(html);
      },
      error: function(error) {
        $("#weather").html('<p>'+error+'</p>');
      }
    });
  });
  */
</script>
@endsection