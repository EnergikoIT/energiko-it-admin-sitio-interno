<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Documento sin título</title>
<style type="text/css">
  h1{ 
    padding:0px; 
    margin:0px;
  }
  body{ 
    background:#f3f3f3;
  }
  .container{
    margin:auto;
    background:#FFF;
    height:100%;
    width:80%; 
    border-radius:8px;
  }
  .container .header{
  	padding: 0px 20px; 
    height:60px; 
    line-height:70px;
  }
  .container .content{
  	 min-height:500px;
  }
  .container .content p{
  	padding: 0px 20px;
  }
  .container .footer{
  	background:#000; 
    color:#FFF;
    padding: 25px 25px; 
    height:25px; 
    border-radius:0px 0px 8px 8px;
  }
</style>
</head>

<body>
<div class="container">
	<div class="header"><img src="{{url('img/mails/hg_logo.png')}}" height="60" style="float:right; margin:5px 0px;"/>
   	  <h1>Contacto HAPPY GO</h1>
    </div>
    
    <div class="content">
    <img src="{{url('img/mails/header-oportunidades.jpg')}}" width="100%"/><br />
    <p>Un nuevo Curriculm ha sido recibido en la bolsa de trabajo de <a href="http://happygo.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://happygo.com&amp;source=gmail&amp;ust=1495559810175000&amp;usg=AFQjCNFWnqjwKU-i_0MObhXx9oCyf6desg">happygo.com</a></p>

    <p>Los datos que el aplicante ingreso son los siguientes:</p>
    
     <p>
      <strong>Nombre:</strong> {{$request->name}}<br />
      <strong>Telefono:</strong> {{$request->phone}} <br />
    <strong>Mensaje:</strong> <em>{{$request->message}}</em></div>
    </p>
    <div class="footer">
    <span style="float:left">Una empresa de Grupo Energiko 2017</span>
    <span style="float:right"><img src="{{url('img/logo-energiko.png')}}" width="116" /></span> </div>
</div>
</body>
</html>
