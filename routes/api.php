<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'apiController@logIn');
Route::get('/eb', 'apiController@earlyBird');

Route::get('/jobs/{div?}/{loc?}/{tp?}', 'apiController@jobs');
Route::post('/contact', 'apiController@contact');

Route::group(['prefix' => 'hotel'], function () {
    Route::get('/earlybird', 'apiController@earlyBird')->middleware('auth:api');
    Route::get('/ebmonthhistory', 'apiController@ebMonthHistory');
    Route::get('/comparative/{period?}', 'apiController@hotelComparative');
});
