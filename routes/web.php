<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/
//Prueba
Route::get('/', function () {
    return view('login');
});

Route::get('home', function () {
    return view('index');
});

Route::group(['prefix' => 'ajax'], function () {
    Route::get('/facturaspendientes/{store_id}', 'ajaxController@pendingInvoices');
});

Route::group(['prefix' => 'contabilidad'], function () {
    Route::get('/cortesz', 'accountancyController@cortesZ');
    Route::get('/tiendas', 'accountancyController@estatusTiendas');
    Route::get('/reportes', 'accountancyController@reportesHome');
    Route::get('/analisis', 'accountancyController@analisis');
    
    Route::post('reportes/vcf', 'reportController@ticketsFacturados');
    Route::post('reportes/existencias', 'reportController@existencias');
    Route::post('reportes/ddm', 'reportController@diarioDeMovimientos');
    Route::post('reportes/fsa', 'reportController@facturasSinAfectar');
    Route::get('reportes/anexos', 'reportController@anexosMov');
    Route::get('reportes/articulos', 'reportController@listaArticulos');
    Route::post('reportes/facturas', 'reportController@facturasTienda');
});

Route::group(['prefix' => 'herramientas'], function () {
    Route::get('/compras/nasoc', 'comprasController@pendingAsoc');
    Route::get('/compras/asoc', 'comprasController@alreadyAsoc');
});

Route::group(['prefix' => 'rh'], function () {
    Route::get('/vacantes', 'rhController@index');
    Route::get('/vacante/{id}/editar', 'rhController@edit');
    Route::get('/vacante/nueva', 'rhController@create');

    Route::post('/vacante/guardar', 'rhController@guardar');
    Route::post('/vacante/actualizar', 'rhController@actualizar');
    Route::post('/vacante/eliminar', 'rhController@eliminar');
});

Route::group(['prefix' => 'crons'], function () {
    Route::get('/mail/cz', 'cronsController@czAlertMail');
    Route::get('/mail/invoices', 'cronsController@invoicesAlertMail');
    Route::get('/mail/agents', 'cronsController@AlertAgents');
    Route::get('/mail/transactionsuspend', 'cronsController@AlertTransactionSuspend');
    Route::get('/mail/admindisa', 'cronsController@AlertaAdminDISA');
    Route::get('/mail/openadmindisa', 'cronsController@AlerOpenAdminDISA');
    Route::get('/mail/erroradmindisa', 'cronsController@AlertErrorAdminDISA');
    Route::get('/mail/duplicadoadmindisa', 'cronsController@AlertDuplicadoaAdminDISA');
    Route::get('/mail/timbrado', 'cronsController@AlertTimbrado');

    
});

//Simple redireccionamiento ya que originalmente se habia dado esta url a varios usuarios
Route::get('estatus/tiendas', function () {
    return redirect('contabilidad/tiendas');
});