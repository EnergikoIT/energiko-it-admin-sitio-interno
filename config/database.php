<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'compucaja' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.96',
            'port' => '1433',
            'database' => 'Compucaja',
            'username' => 'sa',
            'password' => 'Pa$$word0',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'disaapp' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.96',
            'port' => '56248',
            'database' => 'DISAAPP',
            'username' => 'PortalAlertas',
            'password' => 'Pa$$word0',
            'charset' => 'utf8',
            'prefix' => '',
        ],
        'disasys' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.96',
            'port' => '56248',
            'database' => 'DISASYS',
            'username' => 'PortalAlertas',
            'password' => 'Pa$$word0',
            'charset' => 'utf8',
            'prefix' => '',
        ],


        'intelisis' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.71',
            'port' => '1433',
            'database' => 'Energiko',
            'username' => 'sa',
            'password' => 'Energiko123',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'interfaz_ic' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.71',
            'port' => '1433',
            'database' => 'InterfazIC',
            'username' => 'sa',
            'password' => 'Energiko123',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'arpon' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.1.82',
            'port' => '1433',
            'database' => 'Front',
            'username' => 'sa',
            'password' => 'Sacompusis2017',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'arpon-front-auditoria' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.1.82',
            'port' => '1433',
            'database' => 'Front_Auditoria',
            'username' => 'sa',
            'password' => 'Sacompusis2017',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'intranet' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.96',
            'port' => '1433',
            'database' => 'intranet_test',
            'username' => 'sa',
            'password' => 'Pa$$word0',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'potogas' => [
            'driver' => 'sqlsrv',
            'host' => '10.1.100.65',
            'port' => '50905',
            'database' => 'POTOGASAPP',
            'username' => 'sa',
            'password' => 'Pa$$word0',
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
