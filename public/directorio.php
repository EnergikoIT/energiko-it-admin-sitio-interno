﻿<?php

/*
Este arreglo contiene la informacion de los gerentes a los cuales se les enviara un correo electronico
cuando no se genere un corte Z o no se timbren facturas 

'Tienda en Compucaja' => 'Email Gerente Local, Email Gerente Zona'

*/ 


$directorio = array(
    '3' => 'penjamo@happygo.com.mx,iquintero@happygo.com.mx', //LA PIEDAD KOPLA
    '5' => 'kopla@happygo.com.mx,owilliams@happygo.com.mx', //MATRIZ KOPLA
    '6' => 'lacinta1@happygo.com.mx,owilliams@happygo.com.mx', //1 LA CINTA KOPLA
    '7' => 'lacinta2@happygo.com.mx,owilliams@happygo.com.mx', //2 LA CINTA KOPLA
    '8' => 'valtierrilla1@happygo.com.mx,owilliams@happygo.com.mx', //1 VALTIERRILLA KOPLA
    '9' => 'valtierrilla2@happygo.com.mx,owilliams@happygo.com.mx', //2 VALTIERRILLA KOPLA
    '10' => 'metropolitano@happygo.com.mx,iquintero@happygo.com.mx', //METROPOLITANO SIGLO
    '11' => 'sanfrancisco@happygo.com.mx,iquintero@happygo.com.mx', //SAN FRANCISCO II KOPLA
    '12' => 'sanmiguel@happygo.com.mx,aamezquita@happygo.com.mx', //SAN MIGUEL SIGLO
    '14' => 'jalos@happygo.com.mx,aamezquita@happygo.com.mx', //JALOS SIGLO
    '15' => 'santamargarita@happygo.com.mx,aamezquita@happygo.com.mx', //S MARGARITA SIGLO
    '19' => 'lapaz@happygo.com.mx,iquintero@happygo.com.mx', //LA PAZ KOPLA
    '20' => 'kopla@happygo.com.mx,iquintero@happygo.com.mx', //ALAMEDA KOPLA
    '27' => 'sancarlos@happygo.com.mx,iquintero@happygo.com.mx', //SAN CARLOS HGRET
    '28' => 'lahuerta@happygo.com.mx,iquintero@happygo.com.mx', //LA HUERTA HGRET
    '29' => 'santateresita@happygo.com.mx,iquintero@happygo.com.mx', //SANTA TERESITA HGRET
    '30' => 'capilla@happygo.com.mx,iquintero@happygo.com.mx', //CAPILLA HGRET
    '31' => 'sanjorge@happygo.com.mx,iquintero@happygo.com.mx', //SAN JORGE HGRET
    '32' => 'rzavala@happygo.com.mx,aamezquita@happygo.com.mx', //MARIANO OTERO HGRET
    '33' => 'ysaikale@happygo.com.mx,aamezquita@happygo.com.mx' //PERIFERICO HGRET
);

$dir_tim = array(
       'DPG840301KFA'=>'amedrano@energiko.com', //DISTRIBUIDORA POTOSINA DE GAS
       'DIN001226C87'=>'rmartinez@energiko.com', //OPERADORA DISA 
       'SLG480705G6A'=>'amedrano@energiko.com', //SAN LUIS GAS SA DE CV
       'CGA9810197C5'=>'amedrano@energiko.com', // CORPO GAS SA DE CV
       'CCS000127521'=>'amonreal@energiko.com', // SAN JORGE
       'GAS110804J93'=>'amonreal@energiko.com', // GASO AS
       'KOP940610D11'=>'amonreal@energiko.com', // KOPLA
       'SSX990909BW7'=>'amonreal@energiko.com', // SIGLO XXI
       'STO081218RS1'=>'amonreal@energiko.com' // SAN TORIBIO
       
       
   );

$empresas = array(
    array('RFC'=>'DPG840301KFA','Usuario'=>'potogascfdi@potogas.com','Password'=>'DpG_8403.kfA','Empresa'=>'DISTRIBUIDORA POTOSINA DE GAS SA DE CV','Limite'=>'28000'),
    array('RFC'=>'DIN001226C87','Usuario'=>'rmartinez@energiko.com','Password'=>'DI.01.nc','Empresa'=>'OPERADORA DISA SA DE CV','Limite'=>'9688'),
    array('RFC'=>'SLG480705G6A','Usuario'=>'sanluisgascfdi@potogas.com','Password'=>'SlG.4807.g6A','Empresa'=>'SAN LUIS GAS SA DE CV','Limite'=>'100'),
    array('RFC'=>'CGA9810197C5','Usuario'=>'corpogascfdi@potogas.com','Password'=>'CgA.9810.7c5','Empresa'=>'CORPOGAS SA DE CV','Limite'=>'100'),
    array('RFC'=>'CCS000127521','Usuario'=>'cfdi.sanjorge@energiko.com','Password'=>'CC.01.so','Empresa'=>'SAN JORGE','Limite'=>'0'),
    array('RFC'=>'GAS110804J93','Usuario'=>'cfdi.gasoas@energiko.com','Password'=>'GA.01.sj','Empresa'=>'GASO AS','Limite'=>'0'),  
    array('RFC'=>'KOP940610D11','Usuario'=>'cfdi.kopla@energiko.com','Password'=>'Fact.123.123','Empresa'=>'KOPLA','Limite'=>'0'),
    array('RFC'=>'SSX990909BW7','Usuario'=>'cfdi.sigloxxi@energiko.com','Password'=>' Fact123.321%','Empresa'=>'SIGLO XX1','Limite'=>'0'),
    array('RFC'=>'STO081218RS1','Usuario'=>'cfdi.santoribio@energiko.com','Password'=>'StO.0812.rS1','Empresa'=>'SAN TORIBIO','Limite'=>'0')    
   );

$team_aplicaciones = array(
array('Nombre'=>'Reina Martinez','Correo'=>'rmartinez@energiko.com','HoraInicial'=>'08:00:00','HoraFinal'=>'17:30:00','Extra'=>'19:01:00'),
array('Nombre'=>'Alejandra Alejo','Correo'=>'aalejo@energiko.com','HoraInicial'=>'08:00:00','HoraFinal'=>'17:30:00','Extra'=>'19:01:00'),
array('Nombre'=>'Elizabeth Monrreal','Correo'=>'amonreal@energiko.com','HoraInicial'=>'10:00:00','HoraFinal'=>'19:00:00','Extra'=>'19:01:00')
);